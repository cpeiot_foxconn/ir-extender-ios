//
//  remote_air.swift
//  IR Extender
//
//  Created by Foxconn Cic_rc_vol_down_normal.pngPEIOT on 2015/7/8.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import Foundation
class Remote_Air:UIViewController{
    
    @IBOutlet weak var ac_power: UIButton!
    @IBOutlet weak var ac_temp_up: UIButton!
    @IBOutlet weak var ac_temp_down: UIButton!
    @IBOutlet weak var ac_mode: UIButton!
    @IBOutlet weak var ac_fan: UIButton!
    @IBOutlet weak var ac_swing: UIButton!
    @IBOutlet weak var ac_temper: UIButton!
    @IBOutlet weak var codenum: UILabel!
    @IBOutlet weak var ac_panel: remote_acstatus!
    
    var NotifyButtonClick: NotifyButtonClickDelegate!
    
    var m_pRemote: Remote!
    var pickerData = [""]
    
    var CurrentStatus: RemoteKey_AC
        {
        get {
            return m_pCurremtStatus
        }
        set {
            m_pCurremtStatus = newValue
            
            // set mode
            switch m_pCurremtStatus.ModeStatus
            {
            case RemoteKey_AC.AIRMODE.AUTO:
                ac_mode.setImage(UIImage(named: "my_mode_auto_normal.png"),  forState: UIControlState.Normal)
            case RemoteKey_AC.AIRMODE.HEAT:
                ac_mode.setImage(UIImage(named: "ic_rc_ar_heat_normal.png"),  forState: UIControlState.Normal)
            case RemoteKey_AC.AIRMODE.COOL:
                ac_mode.setImage(UIImage(named: "ic_rc_ar_cool_normal.png"),  forState: UIControlState.Normal)
            case RemoteKey_AC.AIRMODE.DRY:
                ac_mode.setImage(UIImage(named: "ic_rc_ar_dehumidify_normal"),  forState: UIControlState.Normal)
            case RemoteKey_AC.AIRMODE.FAN:
                ac_mode.setImage(UIImage(named: "ic_rc_ar_fan.png"),  forState: UIControlState.Normal)
            default :
                println("Setting AC Remote Mode Error!")
            }
            
            // set fan
            switch m_pCurremtStatus.Fanstatus
            {
            case RemoteKey_AC.AIRFAN.AUTO:
                ac_fan.setImage(UIImage(named: "my_fan_auto_normal.png"),  forState: UIControlState.Normal)
            case RemoteKey_AC.AIRFAN.HI:
                ac_fan.setImage(UIImage(named: "ic_rc_ar_fan_heigh_normal.png"),  forState: UIControlState.Normal)
            case RemoteKey_AC.AIRFAN.MID:
                ac_fan.setImage(UIImage(named: "ic_rc_ar_fan_me_normal.png"),  forState: UIControlState.Normal)
            case RemoteKey_AC.AIRFAN.LOW:
                ac_fan.setImage(UIImage(named: "ic_rc_ar_fan_low.png"),  forState: UIControlState.Normal)
            default :
                println("Setting AC Remote Fan Error!")
            }
            
            // set power
            if m_pCurremtStatus.Power == true
            {
                ac_power.setImage(UIImage(named: "ic_rc_ar_power_normal.png"),  forState: UIControlState.Normal)
            }
            else
            {
                ac_power.setImage(UIImage(named: "ic_rc_power_pressed.png"),  forState: UIControlState.Normal)
            }
            
            // set swing
            if m_pCurremtStatus.Swing == true
            {
                ac_swing.setImage(UIImage(named: "ic_rc_ar_auto_normal.png"),  forState: UIControlState.Normal)
            }
            else
            {
                ac_swing.setImage(UIImage(named: "ic_rc_ar_auto_pressed.png"),  forState: UIControlState.Normal)
            }
            
            ac_temper.setTitle(String(m_pCurremtStatus.Temperature), forState: UIControlState.Normal)
        }
    }
    var m_pCurremtStatus: RemoteKey_AC = RemoteKey_AC()
    var templist : Array<Int>!
    var temp_max = -1
    var temp_min = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ac_temper.setTitle(String(m_pCurremtStatus.Temperature), forState: UIControlState.Normal)
        ac_panel.Set_Status(CurrentStatus, IgnoreMode: false, IgnoreFan: false, IgnoreSwing: false)
    }
    
    func initialObject()
    {
        templist = Array<Int>()
        
        for x in m_pRemote.ACKeys
        {
            if(templist.filter{ e in e == x.Temperature}.count > 0)
            {
                continue
            }
            
            templist.append(x.Temperature)
            if(temp_max < x.Temperature)
            {
                temp_max = x.Temperature
            }
            if(temp_min > x.Temperature)
            {
                temp_min = x.Temperature
            }
        }
        m_pCurremtStatus = m_pRemote.ACKeys[0]
        templist.sort({$0 > $1})
    }
    
    @IBAction func temp_up_click(sender: UIButton) {
        if(m_pCurremtStatus.Temperature + 1 <= temp_max)
        {
            m_pCurremtStatus.Temperature = m_pCurremtStatus.Temperature + 1
        }
        CurrentStatus = CurrentStatus.Copy()
        
        if let notify = NotifyButtonClick
        {
            notify.NotifyButtonClick(sender)
        }
    }
    
    @IBAction func temp_down_click(sender: UIButton) {
        if(m_pCurremtStatus.Temperature - 1 >= temp_min)
        {
            m_pCurremtStatus.Temperature = m_pCurremtStatus.Temperature - 1
        }
        CurrentStatus = CurrentStatus.Copy()
        
        if let notify = NotifyButtonClick
        {
            notify.NotifyButtonClick(sender)
        }
    }
    
    @IBAction func mode_click(sender: UIButton) {
        switch m_pCurremtStatus.ModeStatus
        {
        case RemoteKey_AC.AIRMODE.AUTO:
            m_pCurremtStatus.ModeStatus = RemoteKey_AC.AIRMODE.COOL
        case RemoteKey_AC.AIRMODE.COOL:
            m_pCurremtStatus.ModeStatus = RemoteKey_AC.AIRMODE.DRY
        case RemoteKey_AC.AIRMODE.DRY:
            m_pCurremtStatus.ModeStatus = RemoteKey_AC.AIRMODE.FAN
        case RemoteKey_AC.AIRMODE.FAN:
            m_pCurremtStatus.ModeStatus = RemoteKey_AC.AIRMODE.HEAT
        case RemoteKey_AC.AIRMODE.HEAT:
            m_pCurremtStatus.ModeStatus = RemoteKey_AC.AIRMODE.AUTO
        default:
            m_pCurremtStatus.ModeStatus = RemoteKey_AC.AIRMODE.AUTO
        }
        CurrentStatus = CurrentStatus.Copy()
        
        if let notify = NotifyButtonClick
        {
            notify.NotifyButtonClick(sender)
        }
    }
    
    @IBAction func fan_click(sender: UIButton) {
        switch m_pCurremtStatus.Fanstatus
        {
        case RemoteKey_AC.AIRFAN.AUTO:
            m_pCurremtStatus.Fanstatus = RemoteKey_AC.AIRFAN.HI
        case RemoteKey_AC.AIRFAN.HI:
            m_pCurremtStatus.Fanstatus = RemoteKey_AC.AIRFAN.MID
        case RemoteKey_AC.AIRFAN.MID:
            m_pCurremtStatus.Fanstatus = RemoteKey_AC.AIRFAN.LOW
        case RemoteKey_AC.AIRFAN.LOW:
            m_pCurremtStatus.Fanstatus = RemoteKey_AC.AIRFAN.AUTO
        default:
            m_pCurremtStatus.Fanstatus = RemoteKey_AC.AIRFAN.AUTO
        }
        CurrentStatus = CurrentStatus.Copy()
        
        if let notify = NotifyButtonClick
        {
            notify.NotifyButtonClick(sender)
        }
    }
    
    @IBAction func temper_click(sender: UIButton) {
        
        let alertView = CustomIOS7AlertView()
        var pickerview = IntPickerView(data: templist)
        
        //alert view的大小會依照containerView的大小改變
        pickerview.frame = CGRectMake(0, 0 ,200, 150)
        
        alertView.containerView = pickerview
        alertView.buttonTitles = ["OK"]
        alertView.onButtonTouchUpInside = {
            (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
            alertView.close()
            
            self.m_pCurremtStatus.Temperature = pickerview.selectTemper
            self.CurrentStatus = self.CurrentStatus.Copy()
            
            if let notify = self.NotifyButtonClick
            {
                notify.NotifyButtonClick(sender)
            }
        }
        
        alertView.show()
    }
    
    
    @IBAction func power_click(sender: UIButton) {
        // set power
        if m_pCurremtStatus.Power == true
        {
            m_pCurremtStatus.Power = false
        }
        else
        {
            m_pCurremtStatus.Power = true
        }
        CurrentStatus = CurrentStatus.Copy()
        
        if let notify = NotifyButtonClick
        {
            notify.NotifyButtonClick(sender)
        }
    }
    
    @IBAction func swing_click(sender: UIButton) {
        // set power
        if m_pCurremtStatus.Swing == true
        {
            m_pCurremtStatus.Swing = false
        }
        else
        {
            m_pCurremtStatus.Swing = true
        }
        CurrentStatus = CurrentStatus.Copy()
       
        if let notify = NotifyButtonClick
        {
            notify.NotifyButtonClick(sender)
        }
    }
    
    //    func GetControllerStatus()->RemoteKey_AC
    //    {
    //        var pReturn = RemoteKey_AC()
    //
    //
    //        return pReturn
    //    }
    //
    //    func SetControllerStatus(key:RemoteKey_AC)
    //    {
    //    
    //    }
}