//
//  remote_base.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/18.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

class Remote_UIBase: UIViewController, NotifyNumPadClickDelegate, NotifyMorePadClickDelegate, NotifyButtonClickDelegate{
    
    @IBOutlet weak var view_top: UIView!
    @IBOutlet weak var view_bottom: UIView!
  
    var top_controller : Remote_TopController!
    var bottom_controller : Remote_BottomController!
    var m_pRemote : Remote!
    var m_pLearningList = Array<RemoteKey>()
    
    var NotifyFunctionBtnClick: NotifyButtonClickDelegate!
    
    //// Flag for "Top View is circle pad"
    var topview_circlepad : Bool
    {
        get{
            return m_topview_circlepad
        }
        set{
            m_topview_circlepad = newValue
            var normalImg : UIImage!
            var selectImg : UIImage!
            
            if m_topview_circlepad
            {
                normalImg = UIImage(named: "ic_rc_vo_number_normal")
                selectImg  = UIImage(named: "ic_rc_vo_number_pressed")
            }
            else
            {
                normalImg = UIImage(named: "ic_rc_keypad_normal")
                selectImg = UIImage(named: "ic_rc_keypad_pressed")
            }
            
            bottom_controller.btn_numpad.setImage(normalImg, forState: UIControlState.Normal)
            bottom_controller.btn_numpad.setImage(selectImg, forState: UIControlState.Selected)
        }
    }
    var m_topview_circlepad: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func NotifyNumPadClick(btn: UIButton!) {
        if(topview_circlepad)
        {
            top_controller.setNumpadView()
            topview_circlepad = false
        }else{
            top_controller.setCirclePadView()
            topview_circlepad = true
        }
    }
    
    func NotifyButtonClick(btn: UIButton!) {
        if let event = NotifyFunctionBtnClick
        {
            event.NotifyButtonClick(btn)
        }
    }
    
    func NotifyMorePadClick(btn: UIButton!) {
        performSegueWithIdentifier("segue_more", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "segue_top")
        {
            top_controller = segue.destinationViewController as! Remote_TopController
        }
        if(segue.identifier == "segue_bottom")
        {
            bottom_controller = segue.destinationViewController as! Remote_BottomController
        }
        if(segue.identifier == "segue_more")
        {
            var more_controller = segue.destinationViewController as! Remote_MoreButtonsController
            more_controller.original_remotekeys = m_pRemote.Keys
        }
        
        if let bottom = bottom_controller
        {
            bottom_controller.NotifyOpenNumPad = self
            bottom_controller.NotifyOpenMorePad = self
            bottom_controller.NotifySendIRCode = self
        }
        if let top = top_controller
        {
            top_controller.NotifySendIRCode = self
        }
    
    }
}
