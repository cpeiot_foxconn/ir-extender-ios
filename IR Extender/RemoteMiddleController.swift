//
//  RemoteDisplayViewController.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/12.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

/*
    用ContainerView顯示RemoteUI_Base
    因為UIView不能直接套用ViewController
*/
class RemoteMiddleController: UIViewController, NotifyButtonClickDelegate {

    var viewcontroller: UIViewController!
    var m_pRemote : Remote!
    var NotifySendIRCode: NotifySendIRCodeDelegate!
    
    var IgnoreMode = false
    var IgnoreFan = false
    var IgnoreSwing = false
    
    var containersize = CGRect()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch m_pRemote.DeviceID
        {
        case GlobalVariable.DEVICE_ID_TV:
            let storyboard = UIStoryboard(name: "ChildController_TV", bundle: nil)
            viewcontroller = storyboard.instantiateViewControllerWithIdentifier("controller_tv") as! Remote_UIBase
        case GlobalVariable.DEVICE_ID_DVD:
            let storyboard = UIStoryboard(name: "ChildController_DVD", bundle: nil)
            viewcontroller = storyboard.instantiateViewControllerWithIdentifier("controller_dvd") as! Remote_UIBase
        case GlobalVariable.DEVICE_ID_STB:
            let storyboard = UIStoryboard(name: "ChildController_DVD", bundle: nil)
            viewcontroller = storyboard.instantiateViewControllerWithIdentifier("controller_dvd") as! Remote_UIBase
        case GlobalVariable.DEVICE_ID_VHS:
            let storyboard = UIStoryboard(name: "ChildController_DVD", bundle: nil)
            viewcontroller = storyboard.instantiateViewControllerWithIdentifier("controller_dvd") as! Remote_UIBase
        case GlobalVariable.DEVICE_ID_AUDIO:
            let storyboard = UIStoryboard(name: "ChildController_DVD", bundle: nil)
            viewcontroller = storyboard.instantiateViewControllerWithIdentifier("controller_dvd") as! Remote_UIBase
        case GlobalVariable.DEVICE_ID_CD:
            let storyboard = UIStoryboard(name: "ChildController_DVD", bundle: nil)
            viewcontroller = storyboard.instantiateViewControllerWithIdentifier("controller_dvd") as! Remote_UIBase
        case GlobalVariable.DEVICE_ID_STREAMING:
            let storyboard = UIStoryboard(name: "ChildController_Streaming_MI", bundle: nil)
            viewcontroller = storyboard.instantiateViewControllerWithIdentifier("controller_streaming_mi") as! Remote_UIBase
        case GlobalVariable.DEVICE_ID_AIRCONDITIONER:
            let storyboard = UIStoryboard(name: "ChildController_Air", bundle: nil)
            viewcontroller = storyboard.instantiateViewControllerWithIdentifier("controller_aircondition") as! Remote_Air
        default:
            println("No such device id\(m_pRemote.DeviceID)")
        }
        
        /*
        addChildViewController可將ViewController先儲存起來
        當需要用的時候再使用addSubView讓其顯示
        顯示的時候還可以設定動畫
        跟Android的fragmentTrasaction有點像
        */
        
        if m_pRemote.DeviceID == GlobalVariable.DEVICE_ID_AIRCONDITIONER
        {
            let controller = viewcontroller as! Remote_Air
            controller.m_pRemote = m_pRemote
            controller.initialObject()
            controller.NotifyButtonClick = self
        }
        else
        {
            let controller = viewcontroller as! Remote_UIBase
            controller.m_pRemote = m_pRemote
            controller.NotifyFunctionBtnClick = self
        }
        
        self.addChildViewController(viewcontroller)
        self.view.addSubview(viewcontroller.view)
    }
    
    func NotifyButtonClick(btn: UIButton!) {
        var strIRData = ""
        
        if let notify = NotifySendIRCode {
            if m_pRemote.DeviceID == GlobalVariable.DEVICE_ID_AIRCONDITIONER
            {
                strIRData =  getProperIRCode()
                let controller = viewcontroller as! Remote_Air
                controller.ac_panel.Set_Status(controller.CurrentStatus, IgnoreMode: IgnoreMode, IgnoreFan: IgnoreFan, IgnoreSwing: IgnoreSwing)
                
                println("準備發送IR Code:\(strIRData)")
                notify.NotifySendIRCode(strIRData, bIsAVDevice: false)
            }
            else
            {
                strIRData =  searchIRData(btn.restorationIdentifier!)
                println("準備發送IR Code:\(strIRData)")
                notify.NotifySendIRCode(strIRData, bIsAVDevice: true)
            }
        }
    }
    
    func searchIRData(var strTag: String) -> String {
        for key in m_pRemote.Keys
        {
            if(key.KeyName == strTag) {
                return key.IRData
            }
        }
        return ""
    }
    
    ///////////////////////////////////////////////
    //  找尋適當的冷氣IR Code
    //
    func getProperIRCode() -> String
    {
        let accontroller = viewcontroller as! Remote_Air
        var acstatus = accontroller.m_pCurremtStatus
        var templist = Array<RemoteKey_AC>()
        var returnlist = Array<RemoteKey_AC>()
        
        IgnoreMode = false
        IgnoreFan = false
        IgnoreSwing = false
        
        // 先篩選出power跟溫度一樣的 ir code
        if(acstatus.Power == false){
            for key in m_pRemote.ACKeys {
                if(key.Power == false) {
                    return key.IRData
                }
            }
            
            println("沒有Power OFF的IR Code")
            return ""
        }
        
        // 篩選溫度
        for key in m_pRemote.ACKeys {
            if(key.Power == true && acstatus.Temperature == key.Temperature)
            {
                templist.append(key)
            }
        }
        
        if(templist.count == 0)
        {
            println("沒有此溫度IR Code")
            return ""
        }
        
        // 篩選風量
        for tempkey in templist {
            if(acstatus.Fanstatus == tempkey.Fanstatus)
            {
                returnlist.append(tempkey)
            }
        }
        
        if(returnlist.count > 0) {
            templist = returnlist
        }else {
            println("Ignore Fan")
            IgnoreFan = true
        }
        
        
        // 篩選模式
        returnlist.removeAll()
        for tempkey in templist {
            if(acstatus.ModeStatus == tempkey.ModeStatus)
            {
                returnlist.append(tempkey)
            }
        }
        
        if(returnlist.count > 0) {
            templist = returnlist
        }else {
            println("Ignore Mode")
            IgnoreMode = true
        }
        
        // 篩選擺頭
        returnlist.removeAll()
        for tempkey in templist {
            if(acstatus.Swing == tempkey.Swing)
            {
                returnlist.append(tempkey)
            }
        }
        
        if(returnlist.count > 0) {
            return returnlist[0].IRData
        }else {
            println("Ignore Swing")
            IgnoreSwing = true
        }
        
        if(templist.count > 0) {
            return templist[0].IRData
        }
        
        return ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        
    }


}
