//
//  MoreButtonCell.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/22.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

class MoreButtonCell: UICollectionViewCell {

    @IBOutlet weak var btn: UIButton!
    var delegetBtnClick: NotifyButtonClickDelegate!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureForCell(cellData:RemoteKey!) {
        self.btn.setTitle(cellData.KeyName, forState: UIControlState.Normal)
//        var img_normal = UIImage(named: ""))
//        var img_pressed = UIImage(named: ""))
//        btn.setImage(<#image: UIImage?#>, forState: <#UIControlState#>)
    }
    
    /* Cell只會抓到button click, 導致無法觸發cell select事件 */
    @IBAction func btnclick(sender: UIButton) {
        if let event = delegetBtnClick
        {
            event.NotifyButtonClick(sender)
        }
    }

}
