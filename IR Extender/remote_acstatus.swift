
//  remote_acstatus.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/7/8.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

//////////////////////////////////////////////
//  Vinly 2015-07-08 Created
//////////////////////////////////////////////
import UIKit

@IBDesignable
class remote_acstatus: UIView {

    var view:UIView!
    
    @IBOutlet weak var status_power: UILabel!
    @IBOutlet weak var status_swing: UILabel!
    @IBOutlet weak var status_fan: UILabel!
    @IBOutlet weak var status_mode: UILabel!
    @IBOutlet weak var status_temper: UILabel!
    

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        
        status_power.text = "ON"
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        //載入remote_cricle_pad.xib
        let bundle = NSBundle(forClass:self.dynamicType)
        let nib = UINib(nibName:"remote_acstatus", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
    func Set_Status(key: RemoteKey_AC, var IgnoreMode: Bool, var IgnoreFan: Bool, var IgnoreSwing: Bool)
    {
        if(key.Power == true)
        {
            status_power.text = "ON"
        }else
        {
            status_power.text = "OFF"
        }
        
        if(key.Swing == true)
        {
            status_swing.text = "ON"
        }else
        {
            status_swing.text = "OFF"
        }
        
        status_temper.text = String(key.Temperature)
        status_mode.text = String(key.ModeStatus.description)
        status_fan.text = String(key.Fanstatus.description.stringByReplacingOccurrencesOfString("FAN ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil))
        
        
        if(IgnoreMode){
            status_mode.text = status_mode.text!+" Ignore"
        }
        
        if(IgnoreFan){
            status_fan.text = status_fan.text!+" Ignore"
        }
        
        if(IgnoreSwing){
            status_swing.text = status_swing.text!+" Ignore"
        }
        
    }
    
    func Get_Status()->RemoteKey_AC
    {
        var pReturn : RemoteKey_AC = RemoteKey_AC()
        
        if(status_power.text == "ON")
        {
            pReturn.Power = true
        }else
        {
            pReturn.Power = false
        }
        
        if(status_swing.text == "ON")
        {
            pReturn.Swing = true
        }else
        {
            pReturn.Swing = false
        }
        
        pReturn.Temperature = status_temper.text!.toInt()!
        pReturn.ModeStatus = RemoteKey_AC.AIRMODE(rawValue: status_mode.text!)!
        pReturn.Fanstatus = RemoteKey_AC.AIRFAN(rawValue: status_fan.text!)!
        
        return pReturn
    }


}