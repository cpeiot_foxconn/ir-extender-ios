//
//  IntPickerViewAlert.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/7/13.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import UIKit

class IntPickerView: UIView, UIPickerViewDataSource, UIPickerViewDelegate  {
    
    var pickerData: Array<Int>!
    var selectTemper : Int = 28
    
    @IBOutlet weak var picker: UIPickerView!
    var view: UIView!
    
    /*****************************
    initial a view
    ******************************/
    
    convenience init(data: Array<Int>)
    {
        self.init()
        self.pickerData = data
        selectTemper = self.pickerData[0]
        setup()
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        addSubview(view)
        picker.dataSource = self
        picker.delegate = self
    }
    
    func loadViewFromNib() -> UIView
    {
        //載入remote_cricle_pad.xib
        let bundle = NSBundle(forClass:self.dynamicType)
        let nib = UINib(nibName:"IntPickerView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    //MARK: Delegates
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return String(pickerData[row])
    }
    
    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = String(pickerData[row])
        var myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 170)!,
            NSForegroundColorAttributeName:UIColor.blueColor()])
        return myTitle
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectTemper = pickerData[row]
    }
    
    //row hight of the UIPickerView
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 45.0
    }
    
    //row width of the UIPickerView
    func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 100
    }

    

}
