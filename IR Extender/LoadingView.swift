//
//  LoadingView.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/7/24.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    var view: UIView!
    
    /*****************************
    initial a view
    ******************************/
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        //載入LoadingView.xib
        let bundle = NSBundle(forClass:self.dynamicType)
        let nib = UINib(nibName:"LoadingView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    



}
