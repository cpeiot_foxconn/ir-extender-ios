//
//  GetDataFromHttp.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/20.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////

import Foundation
import SwiftHTTP

class GetDataFromHttp {
    
    var notifyDataCompletion: NotifyGetDataCompletionDelegate!
    
    func getJSONArrayDataFromHTTP(strURL: String, params: Dictionary<String, AnyObject>?)
    {
        var request = HTTPTask()
        request.responseSerializer = JSONResponseSerializer() //set return as JSON
        println("Start Connection to Server: \(strURL)")
        request.GET(strURL, parameters: params, completionHandler: {(response: HTTPResponse) in
            if let err = response.error {
                println("error: \(err.localizedDescription)")
                
                var alert = UIAlertView()
                alert.title = "Connection Error"
                alert.message = "Wifi or 4G has no connection."
                alert.addButtonWithTitle("OK")
                alert.show()
                
                if let notification = self.notifyDataCompletion
                {
                    ////////////////////////////////////////
                    /// becasue http connection is not main thread
                    /// use dispatch_async method to notify main thread
                    /// 因為http連線不是在main thread
                    /// 所以要用dispatch_async通知main thread
                    dispatch_async(dispatch_get_main_queue(), {
                        //perform all UI stuff here
                        notification.NotifyGetDataCompletion(nil)
                    })
                }
                return
            }
            
            if let res: AnyObject = response.responseObject {
                println("response: \(res)")
                if let notification = self.notifyDataCompletion
                {
                    ////////////////////////////////////////
                    /// becasue http connection is not main thread
                    /// use dispatch_async method to notify main thread
                    /// 因為http連線不是在main thread
                    /// 所以要用dispatch_async通知main thread
                    dispatch_async(dispatch_get_main_queue(), {
                        //perform all UI stuff here
                        notification.NotifyGetDataCompletion(res)
                    })
                }
            }

        })
        
    }
}