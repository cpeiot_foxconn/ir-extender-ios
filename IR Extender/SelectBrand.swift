//
//  SelectBrand.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/26.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

class SelectBrand: UITableViewController, UITableViewDelegate, NotifyGetDataCompletionDelegate, UISearchBarDelegate{

    @IBOutlet var tb_Brand: UITableView!
    
    var m_pRemote: Remote!  //get from SelectDevice class
    
    var getDataObj = GetDataFromHttp()
    var displaylistdatasource: CustomDatasource!
    var oringinal_brand_list = Array<ImageTitleCellData>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //----- User Define : AV, AC
        if(m_pRemote.DeviceID == "-1")
        {
            // 同一個tableview上面可以有多種tableviewcell
            // 可以分別指定class, 利用cellIdentifier對應到tableview datasource
            displaylistdatasource = CustomDatasource(cellData: Array<ImageTitleCellData>(), cellIdentifier: CustomDatasource.textCellIdentifier,
                configureCell:{(cell, celldata) in
                    var tempcell = cell as! ImageTitleCell;
                    tempcell.configureForCell(celldata as! ImageTitleCellData);
            });
            
            var userdefine_ac: ImageTitleCellData = ImageTitleCellData()
            userdefine_ac.Name = "AC Device"
            userdefine_ac.ID = "-2"
            userdefine_ac.ImgSrc = DBManager.DEVICE_IMG_MAPPING()[userdefine_ac.ID]!
            
            var userdefine_av: ImageTitleCellData = ImageTitleCellData()
            userdefine_av.Name = "AV Device"
            userdefine_av.ID = "-1"
            userdefine_av.ImgSrc = DBManager.DEVICE_IMG_MAPPING()[userdefine_av.ID]!
            
            displaylistdatasource.cellData.append(userdefine_ac)
            displaylistdatasource.cellData.append(userdefine_av)
            tb_Brand.rowHeight = 74
            self.title = "User Define"
        
        }
        else{ //------Display Brand Lists
            
            // get brand list
            displaylistdatasource = CustomDatasource(cellData: Array<ImageTitleCellData>(), cellIdentifier: "BasicCell",
                configureCell:{(cell, celldata) in
                    var brandListCell = cell as! UITableViewCell
                    var data = celldata as! ImageTitleCellData
                    brandListCell.textLabel!.text = data.Name
            });
        
            var param_device = Dictionary<String, AnyObject>()
            param_device.updateValue(m_pRemote.DeviceID, forKey: "device")
            getDataObj.getJSONArrayDataFromHTTP(GlobalVariable.url_all_brands, params:param_device)
            getDataObj.notifyDataCompletion = self
            
            // Add Search bar on title
            // Using SearchController will cover navigation controller => no solution! Vinly 20150527
            var search : UISearchBar = UISearchBar(frame: CGRectMake(0, 0, 100, 25))
            self.navigationItem.titleView = search
            search.text = "Search Brand"
            search.delegate = self
        }
        
        tb_Brand.dataSource = displaylistdatasource
        tb_Brand.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

     // MARK: - After getting Brand List from Server
    func NotifyGetDataCompletion(getData: AnyObject?)
    {
        println("\(getData)")
        if let data: AnyObject = getData?.objectForKey("brands")
        {
            //load data into data source
            oringinal_brand_list = Array<ImageTitleCellData>()
            
            for var x = 0 ; x < data.count ; x++
            {
                var deData: ImageTitleCellData = ImageTitleCellData()
                deData.Name = data[x].objectForKey(GlobalVariable.DB_TABLE_BRANDNAME) as! String
                deData.ID = data[x].objectForKey(GlobalVariable.DB_TABLE_BRANDID) as! String
                oringinal_brand_list.append(deData)
            }
            
            displaylistdatasource.cellData = oringinal_brand_list
            
            //refresh tableview data source
            tb_Brand.reloadData()
        }
    }

    // MARK: - Table view data source
    // other function is implement in TableView/DisplayListTableViewDatasource class
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
     // MARK: - Search Bar Display result
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        var resultlist = Array<ImageTitleCellData>()
        
        if searchText == "Search Brand" || searchText == ""
        {
            displaylistdatasource.cellData = oringinal_brand_list
            return
        }
        
        
        for datacell in oringinal_brand_list {
            if datacell.Name.lowercaseString.rangeOfString(searchText.lowercaseString) != nil
            {
                resultlist.append(datacell)
            }
        }
        
        displaylistdatasource.cellData = resultlist
        tb_Brand.reloadData()
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        if searchBar.text == "Search Brand"
        {
            searchBar.text = ""
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        var rowIndex = tb_Brand.indexPathForSelectedRow()?.row
        
        var brand_data: ImageTitleCellData = displaylistdatasource.cellData[rowIndex!] as! ImageTitleCellData
        
        if let nextController = segue.destinationViewController as? SelectCodest
        {
            m_pRemote.BrandID = brand_data.ID
            m_pRemote.BrandName = brand_data.Name
            nextController.m_pRemote = m_pRemote
        }
    }
    

}
