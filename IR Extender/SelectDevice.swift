//
//  SelectDeviceTableViewController.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/21.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////

import UIKit

class SelectDevice: UITableViewController, UITableViewDelegate, NotifyGetDataCompletionDelegate {
    
    @IBOutlet var tb_Device: UITableView!
    
    var getDataObj = GetDataFromHttp()
    var displaylistdatasource: CustomDatasource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        displaylistdatasource = CustomDatasource(cellData: Array<ImageTitleCellData>(), cellIdentifier: CustomDatasource.textCellIdentifier,
            configureCell:{(cell, celldata) in
                var deviceListCell = cell as! ImageTitleCell;
                deviceListCell.configureForCell(celldata as! ImageTitleCellData);
        });
        
        tb_Device.delegate = self
        tb_Device.dataSource = displaylistdatasource
        
        //start getting data
        getDataObj.getJSONArrayDataFromHTTP(GlobalVariable.url_all_devices, params: nil)
        getDataObj.notifyDataCompletion = self
        
        //back button can not be set on storyboard attr
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }
    
    func NotifyGetDataCompletion(getData: AnyObject?)
    {
        //refresh tableview data source
        tb_Device.reloadData()
        
        if let data: AnyObject = getData?.objectForKey("devices")
        {
            //load data into data source
            displaylistdatasource.cellData = Array<ImageTitleCellData>()
            var  imgMappingTable = DBManager.DEVICE_IMG_MAPPING()
            for var x = 0 ; x < data.count ; x++
            {
                var deData: ImageTitleCellData = ImageTitleCellData()
                deData.Name = data[x].objectForKey(GlobalVariable.DB_TABLE_DEVICENAME) as! String
                deData.Subtitle = deData.Name
                deData.ID = data[x].objectForKey(GlobalVariable.DB_TABLE_DEVICEID) as! String
                deData.ImgSrc = imgMappingTable[deData.ID]!
                displaylistdatasource.cellData.append(deData)
            }
            
            //refresh tableview data source
            tb_Device.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data sourceㄥ
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //CODE TO BE RUN ON CELL TOUCH
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        var rowIndex = tb_Device.indexPathForSelectedRow()?.row
        var device_data: ImageTitleCellData = displaylistdatasource.cellData[rowIndex!] as! ImageTitleCellData
        
        if let nextController = segue.destinationViewController as? SelectBrand
        {
            var m_pRemote = Remote()
            m_pRemote.DeviceID = device_data.ID
            m_pRemote.DeviceName = device_data.Name
            
            if(m_pRemote.DeviceID == GlobalVariable.DEVICE_ID_USER_AC || m_pRemote.DeviceID == GlobalVariable.DEVICE_ID_USER_AV)
            {
                m_pRemote.IsUserDefine = true
            }
            
            nextController.m_pRemote = m_pRemote
        }
    }

    
}
