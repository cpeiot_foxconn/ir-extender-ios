//
//  DeviceData.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/25.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import Foundation
/*
    for display list in table view
*/
class ImageTitleCellData{
    var Name: String = ""   // devicename, brand name, ...
    var Subtitle: String = "" //url, description, ...
    var ImgSrc: String = "" // device logo src
    var ID: String = ""     // device id, brand id...
}