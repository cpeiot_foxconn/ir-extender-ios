//
//  ViewController.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/12.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////

import UIKit

class MainViewController: UIViewController, UITableViewDelegate {
   
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tb_LocalRemote: UITableView!
    
    var datasource_localremote: CustomDatasource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //按下menu button讓side menu隱藏或顯示
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        datasource_localremote = CustomDatasource(cellData: Array<ImageTitleCellData>(), cellIdentifier: CustomDatasource.textCellIdentifier,
            configureCell:{(cell, celldata) in
                var deviceListCell = cell as! ImageTitleCell;
                deviceListCell.configureForCell(celldata as! ImageTitleCellData);
        });
        
        tb_LocalRemote.delegate = self
        tb_LocalRemote.dataSource = datasource_localremote
    
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //load local remote table
        GlobalVariable.DBMANAGER.m_array_localremotes = GlobalVariable.DBMANAGER.LoadAllLocalRemotes()
        
        //add to table
        var deviceImgs = DBManager.DEVICE_IMG_MAPPING()
        datasource_localremote.cellData.removeAll()
        for pRemote in GlobalVariable.DBMANAGER.m_array_localremotes
        {
            var data = ImageTitleCellData()
            data.Name = pRemote.Name
            data.Subtitle = "brand name"
            data.ImgSrc = deviceImgs[pRemote.DeviceID]!
            datasource_localremote.cellData.append(data)
        }
        //refresh data
        tb_LocalRemote.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

        //////////////////////////
        // Display Local Remote
        if(segue.identifier == "segue_local_remote")
        {
            var rowIndex = tb_LocalRemote.indexPathForSelectedRow()?.row
            var remote: ImageTitleCellData = datasource_localremote.cellData[rowIndex!] as! ImageTitleCellData
            
            if let nextController = segue.destinationViewController as? SelectRemote
            {
                nextController.m_pRemote = Remote()
                nextController.m_pRemote.Name = remote.Name
                nextController.m_bLocalRemote = true
            }
        }
    }
    
    
}

