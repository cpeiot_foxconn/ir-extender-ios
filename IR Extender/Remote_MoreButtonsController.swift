
//
//  Remote_MoreButtonsController.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/22.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

let reuseIdentifier = "morebutton_cell"

class Remote_MoreButtonsController: UICollectionViewController, NotifyButtonClickDelegate {

    var original_remotekeys : Array<RemoteKey> = Array<RemoteKey>()
    var display_remotekeys: Array<RemoteKey>!
    var COMMON_KEYS = ["POWER", "PLAY", "CH+", "CH-", "STOP", "MUTE", "0","1","2","3","4","5","6","7","8", "9", "MENU", "HOME", "BACK"]
    
    @IBOutlet var cView_morebuttons: UICollectionView!
    
    var notifyButtonClick : NotifyButtonClickDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // filter common remote keys
        display_remotekeys = Array<RemoteKey>()
        
        for key in original_remotekeys {
            var flag = false
            for str in COMMON_KEYS {
                if key.KeyName == str
                {
                    flag = true
                    break
                }
            }
            if flag == false
            {
                display_remotekeys.append(key)
            }
        }
        
        cView_morebuttons.delegate = self
        cView_morebuttons.dataSource = self
        
        /* TEST CODE */
        for index in 1...5
        {
            var mkey = RemoteKey()
            mkey.KeyName = "mycode " + String(index)
            display_remotekeys.append(mkey)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    func NotifyButtonClick(btn: UIButton!)
    {
        if let event =  notifyButtonClick
        {
            event.NotifyButtonClick(btn)
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return display_remotekeys.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! MoreButtonCell
        
        // Configure the cell
        cell.configureForCell(display_remotekeys[indexPath.item])
        cell.delegetBtnClick = self
        return cell
    }
    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
    }
    */
    
    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
