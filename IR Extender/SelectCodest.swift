//
//  SelectCodest.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/27.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
////////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////

import UIKit

class SelectCodest: UITableViewController, UITableViewDelegate, NotifyGetDataCompletionDelegate {
    
    var m_pRemote: Remote!
    var getDataObj = GetDataFromHttp()
    var displaylistdatasource: CustomDatasource!
    var oringinal_code_list = Array<ImageTitleCellData>()
    
    
    @IBOutlet var tb_codest: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        displaylistdatasource = CustomDatasource(cellData: Array<ImageTitleCellData>(), cellIdentifier: CustomDatasource.textCellIdentifier,
            configureCell:{(cell, celldata) in
                var CodestCell = cell as! UITableViewCell
                var data = celldata as! ImageTitleCellData
                CodestCell.textLabel!.text = data.Name
        });
        
        
        // get brand list
        var param_ = Dictionary<String, AnyObject>()
        param_.updateValue(m_pRemote.DeviceID, forKey: "device")
        param_.updateValue(m_pRemote.BrandID, forKey: "brand")
        getDataObj.getJSONArrayDataFromHTTP(GlobalVariable.url_codelist, params:param_)
        getDataObj.notifyDataCompletion = self
        
    
        tb_codest.dataSource = displaylistdatasource
        tb_codest.delegate = self
    }
    
    // MARK: - After getting Brand List from Server
    func NotifyGetDataCompletion(getData: AnyObject?)
    {
        println("\(getData)")
        if let data: AnyObject = getData?.objectForKey("models")
        {
            //load data into data source
            oringinal_code_list = Array<ImageTitleCellData>()
            
            for var x = 0 ; x < data.count ; x++
            {
                var deData: ImageTitleCellData = ImageTitleCellData()
                deData.Name = data[x].objectForKey(GlobalVariable.DB_TABLE_CODENAME) as! String
                deData.ID = data[x].objectForKey(GlobalVariable.DB_TABLE_CODENUM) as! String
                oringinal_code_list.append(deData)
            }
            
            displaylistdatasource.cellData = oringinal_code_list
            
            //refresh tableview data source
            tb_codest.reloadData()
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        var rowIndex = tb_codest.indexPathForSelectedRow()?.row
        
        var code_data: ImageTitleCellData = displaylistdatasource.cellData[rowIndex!] as! ImageTitleCellData
        
        
        if(segue.identifier == "segue_add_remote")
        {
            var selectremote_controller : SelectRemote = segue.destinationViewController as! SelectRemote
            m_pRemote.CodeNum = oringinal_code_list[rowIndex!].ID
            m_pRemote.CodeName = oringinal_code_list[rowIndex!].Name
            selectremote_controller.m_pRemote = m_pRemote
            selectremote_controller.m_bLocalRemote = false
        }
    }


}
