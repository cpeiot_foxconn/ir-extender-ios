//
//  Remote.swift
//  IR_Extender_Demo
//
//  Created by Foxconn CPEIOT on 2015/6/10.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import Foundation
class Remote {
    
    var Name: String = ""
    var DeviceID : String = ""
    var DeviceName : String = ""
    var BrandID : String = ""
    var BrandName : String = ""
    var CodeName : String = ""
    var CodeNum : String = ""
    var IsUserDefine: Bool = false
    var BoardMAC: String = ""
    
    var Keys : Array<RemoteKey> =  Array<RemoteKey>()
    var ACKeys : Array<RemoteKey_AC> =  Array<RemoteKey_AC>()
    
    init()
    {}
}