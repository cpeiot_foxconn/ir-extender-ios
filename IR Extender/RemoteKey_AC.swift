//
//  RemoteKey_AC.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/7/8.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import Foundation

class RemoteKey_AC {
    enum AIRMODE : String, Printable {
        case AUTO = "AUTO"
        case FAN  = "FAN"
        case HEAT = "HEAT"
        case COOL = "COOL"
        case DRY = "DRY"
        
        var description : String {
            return self.rawValue
        }
    }
    enum AIRFAN : String, Printable{
        case AUTO = "FAN AUTO"
        case HI = "FAN HI"
        case MID = "FAN MID"
        case LOW = "FAN LOW"
        
        var description : String {
            return self.rawValue
        }
    }
    
    var ModeStatus: AIRMODE = AIRMODE.AUTO
    var Fanstatus: AIRFAN = AIRFAN.AUTO
    
    var Swing: Bool = true
    var Power: Bool = true
    var Temperature : Int = 28
    var IRData : String = ""
    var KeyID : String = ""
    var KeyName : String = ""
    
    init()
    {
        
    }
    
    func CompareStatus(key:RemoteKey_AC)->Bool
    {
        if(key.Power == self.Power && key.Swing == self.Swing)
        {
            if(key.ModeStatus == self.ModeStatus && key.Fanstatus == self.Fanstatus)
            {
                if(key.Temperature == self.Temperature)
                {
                    return true
                }
            }
            
        }
        return true
    }
    
    func Copy()->RemoteKey_AC
    {
        var pReturn = RemoteKey_AC()
        pReturn.Power = self.Power
        pReturn.Swing = self.Swing
        pReturn.ModeStatus = self.ModeStatus
        pReturn.Fanstatus = self.Fanstatus
        pReturn.IRData = self.IRData
        pReturn.Temperature = self.Temperature
        pReturn.KeyID = self.KeyID
        
        return pReturn
    }
    
    
    
}