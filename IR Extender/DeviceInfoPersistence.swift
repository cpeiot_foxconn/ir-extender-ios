//
//  DeviceInfo.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/7/20.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import Foundation

class DeviceInfoPersistence: NSObject, NSCoding {
    
    var serviceName: String
    var serviceType: String
    var serviceDomain: String
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("deviceInfoPersistence")
    
    struct PropertyKeys {
        static let serviceNameKey = "serviceName"
        static let serviceTypeKey = "serviceType"
        static let serviceDomainKey = "serviceDomainKey"
    }
    
    init(serviceName:String, serviceType: String, serviceDomain:String){
        self.serviceName = serviceName
        self.serviceType = serviceType
        self.serviceDomain = serviceDomain
        
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(serviceName, forKey: PropertyKeys.serviceNameKey)
        aCoder.encodeObject(serviceType, forKey: PropertyKeys.serviceTypeKey)
        aCoder.encodeObject(serviceDomain, forKey: PropertyKeys.serviceDomainKey)

    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let serviceName = aDecoder.decodeObjectForKey(PropertyKeys.serviceNameKey) as! String
        let serviceType = aDecoder.decodeObjectForKey(PropertyKeys.serviceTypeKey) as! String
        let serviceDomain = aDecoder.decodeObjectForKey(PropertyKeys.serviceDomainKey) as! String

        self.init(serviceName:serviceName, serviceType:serviceType, serviceDomain:serviceDomain)
    }
}