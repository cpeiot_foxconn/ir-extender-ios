//
//  RemoteDisplayViewController.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/12.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

class RemoteViewController: UIViewController, NotifyButtonClickDelegate {

    var viewcontroller_to_display : Remote_UIBase!
    var acviewcontroller_to_display: Remote_Air!
    var m_pRemote : Remote!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
            addChildViewController可將ViewController先儲存起來
            當需要用的時候再使用addSubView讓其顯示
            顯示的時候還可以設定動畫
            跟Android的fragmentTrasaction有點像
        */
        if let controller = viewcontroller_to_display
        {
            self.addChildViewController(controller)
            self.view.addSubview(controller.view)
            controller.view.sizeToFit()
            controller.m_pRemote = m_pRemote
            viewcontroller_to_display.NotifyFunctionBtnClick = self
        }
        
        if let controller = acviewcontroller_to_display
        {
            self.addChildViewController(controller)
            self.view.addSubview(controller.view)
            controller.view.sizeToFit()
            controller.m_pRemote = m_pRemote
        }
    
        
        //acviewcontroller_to_display.
    }
    
    func NotifyButtonClick(btn: UIButton!) {

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        
    }


}
