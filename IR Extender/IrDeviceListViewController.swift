//
//  IrDeviceListViewController.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/30.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Henry 2015-06-29 Created
//////////////////////////////////////////////
import UIKit

class IrDeviceListViewController: UITableViewController, UITableViewDelegate, NotifyMdnsCompletionDelegate,NSNetServiceBrowserDelegate, NSNetServiceDelegate {

    let IRDATA = "262A03C88801901ABA96462C792C64C27058486602DC63C8882101470010009378000000786F00004663006F467800786F46636F6300000000000000000000000000000000000000000000000000000000"
    
    @IBOutlet weak var bar_refresh: UIBarButtonItem!
    @IBOutlet weak var deviceListView: UITableView!
    
    var recentDevices:NSMutableDictionary!  //henry20150706
    var devices:NSMutableDictionary!    //henry20150706
    
    var globalConfig:SmartConfigGlobalConfig!   //henry20150706
    var displayListDataSource: CustomDatasource!
    var oringinal_code_list = Array<ImageTitleCellData>()

    var allService: NSMutableArray = NSMutableArray()	//henry20150730, array to store NSNetService for future reference
    var allMQTT: NSMutableArray = NSMutableArray()  //henry20150805, array to store each MQTT connection
    var mdnsService:SmartConfigDiscoverMDNS!
//    var resolvedAddressDictionary = [String:String]()    //henry20150730, empty dictionary for ip address
    //Dictionary Structure -> Device Name:Device IP:
    
//    var deviceInfoDictionary = [String:DeviceInfo]() //henry20150805, dictionary for DeviceInfo for easy access, [deviceName: deviceInfo]
    
//    var deData: ImageTitleCellData = ImageTitleCellData()
    var timeout:NSInteger = 3
    var timer:NSTimer!
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        displayListDataSource = CustomDatasource(cellData: Array<ImageTitleCellData>(), cellIdentifier: CustomDatasource.textCellIdentifier,
            configureCell:{(cell, celldata) in
                var deviceListCell = cell as! ImageTitleCell;
                deviceListCell.configureForCell(celldata as! ImageTitleCellData);
        });
        deviceListView.dataSource = displayListDataSource
        deviceListView.delegate = self
        
        //add WAN node to first element in Arrays, henry20150805
        var deviceInfo = DeviceInfo()
        deviceInfo.ConnectType = DeviceInfo.ConnectionType.WAN
        deviceInfo.isConnecting = true
        Connection.DEVICE_INFO.updateValue(deviceInfo, forKey: GlobalVariable.MQTT_WAN_NAME)
        var mqtt = MQTT_Brain()
        mqtt.setDeviceName(GlobalVariable.MQTT_WAN_NAME)
        mqtt.connectToMQTT()
//        mqtt.publishToMQTT(false, topicType: Connection.topicType.Cmd, toMAC: getMacAddressFromName(getMacAddressFromName(deviceInfo.Name)), data: IRDATA)
        allMQTT.addObject(mqtt as MQTT_Brain)
        
        refresh_mdns()
        
        if(!GlobalVariable.DBMANAGER.GetAllIRServiceList().isEmpty)
        {
            populateDevicesList(GlobalVariable.DBMANAGER.GetAllIRServiceList())    //henry20150724, load saved device list from DB
        }
    }
    
    //henry20150708
    func NotifyMdnsCompletion(getData: AnyObject?)
    {
        getDeviceToPersistence()
        println(">>>>> delegate successful <<<<<")
    }
    
    func refresh_mdns(){
        // start MDNS discovery
        println("start mdns discovery")
//            var timer = NSTimer.scheduledTimerWithTimeInterval(5, target:self, selector: Selector("refresh_mdns"), userInfo: nil, repeats: true)

        mdnsService = SmartConfigDiscoverMDNS.getInstance()
        mdnsService.NotifyMdnsCompletion = self //henry20150713, for delegate
        mdnsService.startMDNSDiscovery("")
    }
    
    //henry20150727, add detected device to db
    func getDeviceToPersistence(){
        println("getDeviceToPersistence")
        globalConfig = SmartConfigGlobalConfig.getInstance()
        devices = globalConfig.getDevices() //gets the information for each detected mdns device, henry20150706
        
        for (key,value) in devices {
            var deviceKey: AnyObject? = devices.objectForKey(key)
            var deviceName = deviceKey?.objectForKey("name") as! String
            var deviceType = deviceKey?.objectForKey("type") as! String
            var deviceDomain = deviceKey?.objectForKey("domain") as! String
            
            //prevents duplicate saving of device if db already exists, henry20150730
            var serviceList = GlobalVariable.DBMANAGER.GetAllIRServiceList()
            var arrayDeviceName =  Array<String>()

            //updates detect duplicate method, henry20150804
            if !serviceList.isEmpty {
                for deviceSet in 0..<serviceList.count{
                    arrayDeviceName.append(serviceList[deviceSet][0])   //extracts device name to new array for comparison
                }
                
                if !contains(arrayDeviceName, deviceName) {
                    GlobalVariable.DBMANAGER.SaveIRService(deviceName, type: deviceType, domain: deviceDomain)
                    populateDevicesList(GlobalVariable.DBMANAGER.GetAllIRServiceList())
                }
            }
            else {  //saves all detected device to db if db is empty
                GlobalVariable.DBMANAGER.SaveIRService(deviceName, type: deviceType, domain: deviceDomain)
                populateDevicesList(GlobalVariable.DBMANAGER.GetAllIRServiceList())
            }
        }
    }
    
    //henry20150724, populate device list from saved devices
    func populateDevicesList(arraySavedDeviceInfo:Array<Array<String>>)
    {
        displayListDataSource?.cellData = Array<ImageTitleCellData>()
        
        for deviceSet in 0..<arraySavedDeviceInfo.count{
            var serviceName = arraySavedDeviceInfo[deviceSet][0]
            var serviceType = arraySavedDeviceInfo[deviceSet][1]
            var serviceDomain = arraySavedDeviceInfo[deviceSet][2]
            println("deviceSet \(deviceSet)")
            //henry20150804
            var deviceInfo = DeviceInfo()
            deviceInfo.Name = serviceName
            Connection.DEVICE_INFO.updateValue(deviceInfo, forKey: serviceName)
            
            //henry20150727, resolve address from saved device
            println("Resolving:\(serviceName) \(serviceType) \(serviceDomain)")
            
            var service: NSNetService!
            service = NSNetService(domain: serviceDomain, type: serviceType, name: serviceName)
            service.delegate = self
            service.resolveWithTimeout(GlobalVariable.TIMEOUT)
            
            allService.addObject(service as NSNetService)   //henry20150730, adds a NSNetService to array for future access
            
            var deData: ImageTitleCellData = ImageTitleCellData()
            deData.Name = serviceName
            deData.Subtitle = NSLocalizedString("ResolvingAddress", comment: "Resolving Address Message")
            deData.ImgSrc = "ic_device.png"
            displayListDataSource?.cellData.append(deData)
            }
        deviceListView?.reloadData()
    }
    
    //henry20150729, updates the subtitle for dyanmic information
    func updateSubtitle(serviceName:String, subtitle:String) {
        for index in 0..<displayListDataSource.cellData.count {
            var cellData = displayListDataSource.cellData[index] as! ImageTitleCellData

            if cellData.Name == serviceName {
                cellData.Subtitle = subtitle
                deviceListView?.reloadData()
                println("updateSubtitle \(index) \(cellData.Name) \(cellData.Subtitle)")
            }
        }
    }
    
    //henry20150727, Prints the IP of the address after successful resolve
    func netServiceDidStop(sender: NSNetService) {
        println("netService Resolve Stopped \(sender.name)")
        
        //        timer.invalidate()  //henry20150728, stops the timer when resolves stops
        println("\(sender.name) has address \(Connection.DEVICE_INFO[sender.name]?.Address)")
        updateSubtitle(sender.name, subtitle: Connection.DEVICE_INFO[sender.name]!.Address)
        
        var deviceInfo = Connection.DEVICE_INFO[sender.name]
        deviceInfo!.ConnectType = DeviceInfo.ConnectionType.LAN
        deviceInfo!.isConnecting = true
        deviceInfo!.BoardMAC = Connection.getMacAddressFromName(sender.name)
        Connection.DEVICE_INFO.updateValue(deviceInfo!, forKey: sender.name)
        
        println("nnnname \(sender.name) \(Connection.DEVICE_INFO[sender.name]?.Name)")
        //henry20150804, connect to LAN MQTT when IP is resolved
        
        var mqtt = MQTT_Brain()
        mqtt.setDeviceName(sender.name)
        mqtt.connectToMQTT()
        allMQTT.addObject(mqtt as MQTT_Brain)
        
//        allService.removeLastObject()   //remove last used service thread from array
    }
    //henry20150727, resolves the IP address of the service
    func netServiceDidResolveAddress(sender: NSNetService) {
        mdnsService = SmartConfigDiscoverMDNS.getInstance()
        var resolvedAddress = mdnsService.resolveAddress(sender.addresses).stringByReplacingOccurrencesOfString("http://", withString: "")  //remove http:// header, henry20150730
        resolvedAddress = resolvedAddress.stringByReplacingOccurrencesOfString(":80", withString: "")   //remove port 80 header
        
        //henry20150805, updates address to dictionary respectively
        var deviceInfo = Connection.DEVICE_INFO[sender.name]
        deviceInfo!.Address = resolvedAddress
        Connection.DEVICE_INFO.updateValue(deviceInfo!, forKey: sender.name)
        
        println("netService Resolve Address \(sender.name) has resolved IP Address:\(Connection.DEVICE_INFO[sender.name]?.Address)")
    }
    
    //henry20150727, Prints error message after unsuccessful resolve
    func netService(sender: NSNetService, didNotResolve errorDict: [NSObject : AnyObject]) {
        println("netService Resolve Error! \(sender.name)")
        
        var mqtt_wan = allMQTT[0] as! MQTT_Brain  //+1 because 0 is always WAN, henry20150805
        if mqtt_wan.isConnected() {
            updateSubtitle(sender.name, subtitle: NSLocalizedString("ResolveAddressWAN", comment: "Connected to WAN when Local IP not avaliable"))
        }
        else {
            updateSubtitle(sender.name, subtitle: NSLocalizedString("ResolveAddressFailed", comment: "Resolve Address Failure Message"))
        }

        //henry20150805, change status to WAN if local IP not avaliable
        var deviceInfo = Connection.DEVICE_INFO[sender.name]
        deviceInfo!.ConnectType = DeviceInfo.ConnectionType.WAN
        deviceInfo!.isConnecting = true
        deviceInfo!.BoardMAC = Connection.getMacAddressFromName(sender.name)
        Connection.DEVICE_INFO.updateValue(deviceInfo!, forKey: sender.name)
        
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //CODE TO BE RUN ON CELL TOUCH
    }
    
    //henry20150803, segue to Remote List
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showRemote" {
            var indexPath = deviceListView.indexPathForSelectedRow()!.row
            var destViewController = segue.destinationViewController as! RemoteListViewController
            var cellData = displayListDataSource.cellData[indexPath] as! ImageTitleCellData
            
            //henry20150805, get mqtt object respective to deviceName
            var mqtt = MQTT_Brain()
            //henry20150807, consider connectType to avoid array out of bound
            if Connection.DEVICE_INFO[cellData.Name]?.ConnectType.rawValue == "WAN" {
                            mqtt = allMQTT[0] as! MQTT_Brain
            }
            else {
                mqtt = allMQTT[indexPath+1] as! MQTT_Brain  //+1 because 0 is always WAN, henry20150805
            }
            mqtt.publishToMQTT(false, topicType: Connection.topicType.Cmd, toMAC: Connection.getMacAddressFromName(cellData.Name), data: IRDATA)
            destViewController.deviceName = cellData.Name
        }
    }
    
    

    
    

    
}