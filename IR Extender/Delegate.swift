//
//  Delegate.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/21.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import Foundation
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//  公用delegate型態定義檔案
//////////////////////////////////////////////

//////for get data from http
protocol NotifyGetDataCompletionDelegate{
    func NotifyGetDataCompletion(getData: AnyObject?)
}

//////for remote contorl
protocol NotifyNumPadClickDelegate{
    func NotifyNumPadClick(btn: UIButton!)
}

protocol NotifyMorePadClickDelegate{
    func NotifyMorePadClick(btn: UIButton!)
}

protocol NotifyButtonClickDelegate{
    func NotifyButtonClick(btn: UIButton!)
}


//////for get device list from mdns, henry20150706
@objc public protocol NotifyMdnsCompletionDelegate{
    func NotifyMdnsCompletion(getData: AnyObject?)
}
protocol NotifySendIRCodeDelegate{
    func NotifySendIRCode(strIRData: String, bIsAVDevice: Bool)
}

protocol NotifyEventDelegate{
    func NotifyEventCompelete()
}

