//
//  remote_circle_pad.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/8.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

@IBDesignable
class remote_circle_pad: UIView {

    @IBOutlet weak var btn_up: UIButton!
    @IBOutlet weak var btn_down: UIButton!
    @IBOutlet weak var btn_left: UIButton!
    @IBOutlet weak var btn_right: UIButton!
    @IBOutlet weak var btn_center: UIButton!

    @IBOutlet weak var circle_img: UIImageView!
    var view:UIView!
    
    var NotifySendIRCode: NotifyButtonClickDelegate!
    
    /*****************************
        initial a view
    ******************************/
  
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        //載入remote_cricle_pad.xib
        let bundle = NSBundle(forClass:self.dynamicType)
        let nib = UINib(nibName:"remote_circle_pad", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
    /* button action */
    
    @IBAction func btn_touch_up(sender: UIButton!) {
        circle_img.image = UIImage(named: "ic_keypad.png")
        if let notify = NotifySendIRCode
        {
            NotifySendIRCode.NotifyButtonClick(sender)
        }
    }

    @IBAction func btn_right_touch_down(sender: UIButton!) {
         circle_img.image = UIImage(named: "ic_keypad_right_pressed.png")
        if let notify = NotifySendIRCode
        {
            NotifySendIRCode.NotifyButtonClick(sender)
        }

    }
    @IBAction func btn_down_touch_down(sender: UIButton!) {
        circle_img.image = UIImage(named: "ic_keypad_down_pressed.png")
        if let notify = NotifySendIRCode
        {
            NotifySendIRCode.NotifyButtonClick(sender)
        }

    }
    @IBAction func btn_up_touch_down(sender: UIButton!) {
        circle_img.image = UIImage(named: "ic_keypad_up_pressed.png")
        if let notify = NotifySendIRCode
        {
            NotifySendIRCode.NotifyButtonClick(sender)
        }

    }
    
    @IBAction func btn_left_touch_down(sender: UIButton!) {
        circle_img.image = UIImage(named: "ic_keypad_left_pressed.png")
        if let notify = NotifySendIRCode
        {
            NotifySendIRCode.NotifyButtonClick(sender)
        }

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
