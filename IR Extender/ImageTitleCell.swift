//
//  DeviceTableViewCell.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/25.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

class ImageTitleCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureForCell(cellData:ImageTitleCellData) {
        self.title?.text = cellData.Name
        self.subtitle.text = cellData.Subtitle
        self.img.image = UIImage(named: cellData.ImgSrc)
    }
    

}
