//
//  DisplayListTableViewDataSource.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/22.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import Foundation
import UIKit

class CustomDatasource: NSObject, UITableViewDataSource{

    var cellData =  Array<AnyObject>();
    var cellIdentifier: String?;
    var configureCell: ((AnyObject,AnyObject) -> ())?;  //method也可以當變數  0.0
    
    static var textCellIdentifier = "TextCell"
    
    init(cellData: Array<AnyObject>, cellIdentifier: String, configureCell: (AnyObject, AnyObject) -> ()) {
        super.init()
        self.cellData = cellData
        self.cellIdentifier = cellIdentifier
        self.configureCell = configureCell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellData.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier!, forIndexPath: indexPath) as! UITableViewCell
        
        configureCell!(cell, cellData[indexPath.row])
        
        return cell;
    }
}