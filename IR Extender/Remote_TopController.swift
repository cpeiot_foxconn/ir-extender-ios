//
//  Remote_TV.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/7.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

class Remote_TopController: UIViewController, NotifyButtonClickDelegate{
    
    var circle_pad: UIViewController!
    var num_pad : UIViewController!
    
    var view_circle_pad : remote_circle_pad!
    var view_num_pad : remote_num_pad!

    var NotifySendIRCode : NotifyButtonClickDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        circle_pad = self.storyboard?.instantiateViewControllerWithIdentifier("view_cirlce_pad") as! UIViewController
        
        // 有些device沒有數字鍵盤, 但在storyboard還是要加一個空白的viewcontroller, storyboard id 設為view_num_pad
        num_pad = self.storyboard?.instantiateViewControllerWithIdentifier("view_num_pad") as! UIViewController
        
        // 顯示circle pad
        self.addChildViewController(circle_pad)
        self.view.addSubview(circle_pad.view)
        
        view_circle_pad = circle_pad.view as! remote_circle_pad
        view_num_pad = num_pad.view as! remote_num_pad
        
        view_circle_pad.NotifySendIRCode = self
        view_num_pad.NotifySendIRCode = self
    }
    
    func NotifyButtonClick(btn: UIButton!)
    {
        if let notify = NotifySendIRCode
        {
            notify.NotifyButtonClick(btn)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNumpadView()
    {
        self.view.addSubview(num_pad.view)
        num_pad.view.frame = circle_pad.view.frame
    }
    
    func setCirclePadView()
    {
        self.view.addSubview(circle_pad.view)
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */

}
