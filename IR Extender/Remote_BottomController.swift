//
//  remote_base_controller.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/17.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import UIKit

class Remote_BottomController: UIViewController {

    @IBOutlet weak var btn_power: UIButton!
    @IBOutlet weak var btn_numpad: UIButton!
    @IBOutlet weak var btn_more: UIButton!
    
    @IBOutlet weak var btn_ch_up: UIButton!
    @IBOutlet weak var btn_ch_down: UIButton!
    @IBOutlet weak var btn_vol_up: UIButton!
    @IBOutlet weak var btn_vol_down: UIButton!
    @IBOutlet weak var btn_input: UIButton!
    @IBOutlet weak var btn_home: UIButton!
    @IBOutlet weak var btn_mute: UIButton!
    @IBOutlet weak var btn_menu: UIButton!
    @IBOutlet weak var btn_back: UIButton!
    
    
    @IBOutlet weak var btn_play: UIButton!
    @IBOutlet weak var btn_stop: UIButton!
    
    var NotifySendIRCode : NotifyButtonClickDelegate!
    var NotifyOpenNumPad : NotifyNumPadClickDelegate!
    var NotifyOpenMorePad : NotifyMorePadClickDelegate!
    
    var remotekeys : Array<RemoteKey>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_click(sender: UIButton) {
        if let notify = self.NotifySendIRCode
        {
            notify.NotifyButtonClick(sender)
        }
    }
    
    @IBAction func more_function_page(sender: UIButton) {
        if let notify = self.NotifyOpenMorePad
        {
            notify.NotifyMorePadClick(sender)
        }
    }
    
    @IBAction func numpad_page(sender: UIButton) {
        if let notify = self.NotifyOpenNumPad
        {
            notify.NotifyNumPadClick(sender)
        }
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }


}
