//
//  remote_num_pad.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/17.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//////////////////////////////////////////////
import UIKit

@IBDesignable
class remote_num_pad: UIView {

    var view:UIView!
    
    @IBOutlet weak var right_btn: UIButton!
    @IBOutlet weak var left_btn: UIButton!

    @IBInspectable var right_btn_normalImg: UIImage!
    @IBInspectable var right_btn_selectImg: UIImage!
    @IBInspectable var right_btn_disableImg: UIImage!
    @IBInspectable var left_btn_normalImg: UIImage!
    @IBInspectable var left_btn_selectImg: UIImage!
    @IBInspectable var left_btn_disableImg: UIImage!
    @IBInspectable var right_btn_HorizonInspect: CGFloat = 0
    @IBInspectable var right_btn_VerticalInspect: CGFloat = 0
    @IBInspectable var left_btn_HorizonInspect: CGFloat = 0
    @IBInspectable var left_btn_VerticalInspect: CGFloat = 0
    
    var NotifySendIRCode : NotifyButtonClickDelegate!

    
    /*****************************
    initial a view
    ******************************/
    override func drawRect(rect: CGRect) {
        right_btn.setImage(right_btn_normalImg, forState: UIControlState.Normal)
        right_btn.setImage(right_btn_selectImg, forState: UIControlState.Selected)
        right_btn.setImage(right_btn_disableImg, forState: UIControlState.Disabled)
        
        left_btn.setImage(left_btn_normalImg, forState: UIControlState.Normal)
        left_btn.setImage(left_btn_selectImg, forState: UIControlState.Selected)
        left_btn.setImage(left_btn_disableImg, forState: UIControlState.Disabled)
        
        right_btn.imageEdgeInsets.left = right_btn_HorizonInspect
        right_btn.imageEdgeInsets.right = right_btn_HorizonInspect
        right_btn.imageEdgeInsets.top = right_btn_VerticalInspect
        right_btn.imageEdgeInsets.bottom = right_btn_VerticalInspect
        
        left_btn.imageEdgeInsets.left = left_btn_HorizonInspect
        left_btn.imageEdgeInsets.right = left_btn_HorizonInspect
        left_btn.imageEdgeInsets.top = left_btn_VerticalInspect
        left_btn.imageEdgeInsets.bottom = left_btn_VerticalInspect
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = NSBundle(forClass:self.dynamicType)
        let nib = UINib(nibName:"remote_num_pad", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func numbtn_click(sender: UIButton!) {
        if let notify = NotifySendIRCode
        {
            notify.NotifyButtonClick(sender)
        }
    }
    
//    func setLeftBtnImg(img:UIImage)
//    {
//        self.left_btn.setImage(img, forState: UIControlState.Normal)
//        self.left_btn.setImage(img, forState: UIControlState.Normal)
//        self.left_btn.setImage(img, forState: UIControlState.Normal)
//    }
//    
//    func setRightBtnImg(img:UIImage)
//    {
//        self.left_btn.setImage(img, forState: UIControlState.Normal)
//    }
    
}
