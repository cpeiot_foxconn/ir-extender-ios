//
//  DeviceInfo.swift
//  IR Extender GIT
//
//  Created by Foxconn CPEIOT on 2015/8/4.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//henry20150804

import Foundation

class DeviceInfo {
    
    var Name: String = ""
    var Address : String = ""
    var ConnectType: ConnectionType
    var isConnected: Bool
    var isConnecting: Bool
    var BoardMAC: String = ""

    enum ConnectionType: String {
        case LAN = "LAN"
        case WAN = "WAN"
        case NONE = "NONE"
    }
    
//    init(Name: String, Address: String) {
//        self.Name = Name
//        self.Address = Address
//        self.ConnectType = ConnectionType.NONE
//        self.isConnected = false
//    }

    init() {
        self.Name = GlobalVariable.MQTT_WAN_NAME
        self.Address = GlobalVariable.MQTT_WAN_ADDRESS
        self.ConnectType = ConnectionType.NONE
        self.isConnected = false
        self.isConnecting = false
        self.BoardMAC = GlobalVariable.MQTT_WAN_MAC
    }
    
//    func setConnectionType(type:ConnectionType) {
//        ConnectType = type
//    }
    func getConnectionType() ->String {
        return ConnectType.rawValue
    }
    
}