//
//  SelectRemote.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/11.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-25 Created
//  TODO: Getting remote data from local db or server and pass data to RemoteController
//////////////////////////////////////////////
import UIKit

class SelectRemote: UIViewController, NotifyGetDataCompletionDelegate, NotifyEventDelegate {

    var m_pRemote : Remote!
    var m_bLocalRemote = false
    var getDataObj = GetDataFromHttp()
    var loadingview : CustomIOS7AlertView!
    var m_pRemoteController : RemoteController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getDataObj.notifyDataCompletion = self
        
        let storyboard = UIStoryboard(name: "RemoteController", bundle: nil)
        m_pRemoteController = storyboard.instantiateViewControllerWithIdentifier("view_remote") as! RemoteController
        m_pRemoteController.NotifyBakcToHomePage = self
        
        //////////////////////////////////
        // if add new remote
        if (m_bLocalRemote == false)
        {
            // get key list
            var param_ = Dictionary<String, AnyObject>()
            param_.updateValue(m_pRemote.CodeNum, forKey: "codeset")
            
            if m_pRemote.DeviceID == GlobalVariable.DEVICE_ID_AIRCONDITIONER {
                getDataObj.getJSONArrayDataFromHTTP(GlobalVariable.url_irdata_ac, params:param_)
            }
            else {
                getDataObj.getJSONArrayDataFromHTTP(GlobalVariable.url_irdata_av, params:param_)
            }
            
            loadingview = CustomIOS7AlertView()
            loadingview.containerView = LoadingView()
            loadingview.show()
        }
            //////////////////////////////////
            // if local remote
        else
        {
            m_pRemote = GlobalVariable.DBMANAGER.LoadRemote(m_pRemote.Name)
            m_pRemoteController.m_pRemote = self.m_pRemote
            
            // 拿到remote data後才可以載入reomte controller
            self.addChildViewController(m_pRemoteController)
            self.view.addSubview(m_pRemoteController.view)
        }
    }
    
    
    func NotifyEventCompelete() {
        self.performSegueWithIdentifier("segue_home", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - After getting key List from Server
    func NotifyGetDataCompletion(getData: AnyObject?)
    {
        println("\(getData)")
        
        if let data: AnyObject = getData?.objectForKey("keys")
        {
            //load data into data source
            m_pRemote.Keys = Array<RemoteKey>()
            m_pRemote.ACKeys = Array<RemoteKey_AC>()
            
            // load ac keys
            if m_pRemote.DeviceID == GlobalVariable.DEVICE_ID_AIRCONDITIONER
            {
                for var x = 0 ; x < data.count ; x++
                {
                    var key: RemoteKey_AC = RemoteKey_AC()
                    key.KeyID = data[x].objectForKey(GlobalVariable.DB_TABLE_KEYID) as! String
                    key.IRData = data[x].objectForKey(GlobalVariable.DB_TABLE_IRDATA) as! String
                    
                    var swing = data[x].objectForKey(GlobalVariable.DB_TABLE_SWING) as! String
                    var power = data[x].objectForKey(GlobalVariable.DB_TABLE_POWER) as! String
                    
                    
                    if (swing == "ON"){
                        key.Swing = true
                    }
                    else {
                        key.Swing = false
                    }
                    if (power == "ON"){
                        key.Power = true
                    }
                    else {
                        key.Power = false
                    }
                    
                    key.Temperature = (data[x].objectForKey(GlobalVariable.DB_TABLE_TEMPERATURE) as! String!).toInt()!
                    key.ModeStatus = RemoteKey_AC.AIRMODE(rawValue: data[x].objectForKey(GlobalVariable.DB_TABLE_MODE) as! String)!
                    key.Fanstatus = RemoteKey_AC.AIRFAN(rawValue: data[x].objectForKey(GlobalVariable.DB_TABLE_FAN) as! String)!
                    
                    m_pRemote.ACKeys.append(key)
                }
            }
            else // load av keys
            {
                for var x = 0 ; x < data.count ; x++
                {
                    var key: RemoteKey = RemoteKey()
                    key.KeyName = data[x].objectForKey(GlobalVariable.DB_TABLE_KEYNAME) as! String
                    key.KeyID = data[x].objectForKey(GlobalVariable.DB_TABLE_KEYID) as! String
                    key.IRData = data[x].objectForKey(GlobalVariable.DB_TABLE_IRDATA) as! String
                    m_pRemote.Keys.append(key)
                }
            }
        }
        
        loadingview.close()
        m_pRemoteController.m_pRemote = self.m_pRemote
        
        // 拿到remote data後才可以載入reomte controller
        self.addChildViewController(m_pRemoteController)
        self.view.addSubview(m_pRemoteController.view)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

    }
    

}
