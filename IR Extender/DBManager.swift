//
//  DBManager.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/6/29.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-06-29 Created
//  for saving and loading remote data in local db
//////////////////////////////////////////////
// 使用Squeal注意事項:
//  1. selectFrom方法, 如果table中沒有任何row, 使用 for row in db.selectFrom(....)語法就不會跳進for迴圈中 (網頁範例容易誤導)
//
//
//////////////////////////////////////////////
import Foundation
import Squeal

class DBManager {
    
    enum SaveStatus: String, Printable  {
        case Sucess = "Save Sucess"
        case Repeate = "Save Repeate Fail"
        case Error = "Other Error"
        
        var description : String {
            return self.rawValue
        }
    }
    
    var m_db : Database!
    var m_array_localremotes: Array<Remote>!
    
    static let DATABASE_NAME: String = "remote_data.sqlite"
    static let REMOTE_TABLE: String = "REMOTE_TABLE"
    static let BRAND_TABLE: String = "BRAND_TABLE"
    static let CODESET_TABLE: String = "CODESET_TABLE"
    static let IR_TABLE: String = "IR_TABLE"
    static let IR_AC_TABLE: String = "IR_AC_TABLE"
    static let MODIFY_IRDATA_TABLE: String = "MODIFY_IRDATA_TABLE"
    static let KEY_TABLE: String = "KEY_TABLE"
    static let KEY_AC_TABLE: String = "KEY_AC_TABLE"
    static let SERVICE_TABLE: String = "IRDEVICE_SERVICE_TABLE"
    
    //remote
    static let DBTAG_REMOTENAME: String = "Name"
    static let DBTAG_DEVICEID: String = "DeviceID"
    static let DBTAG_DEVICENAME: String = "DeviceName"
    static let DBTAG_BRANDID: String = "BrandID"
    static let DBTAG_BRANDNAME: String = "BrandName"
    static let DBTAG_CODENUM: String = "CodeNum"
    static let DBTAG_CODENAME: String = "CodeName"
    static let DBTAG_BOARDMAC: String = "BoardMAC"
    static let DBTAG_USERDEFINE: String = "IsUserDefine"
    static let DBTAG_KEYID: String = "KeyID"
    static let DBTAG_KEYNAME: String = "KeyName"
    static let DBTAG_STPOWER: String = "stPower"
    static let DBTAG_STMODE: String = "stMode"
    static let DBTAG_STTEMP: String = "stTemp"
    static let DBTAG_STFAN: String = "stFan"
    static let DBTAG_STSWING: String = "stSwing"
    static let DBTAG_IRDATA: String = "IRData"
    
    //service
    static let DBTAG_SERVICENAME: String = "ServiceName"
    static let DBTAG_SERVICETYPE: String = "Type"
    static let DBTAG_SERVICEDOMAIN: String = "Domain"
    
    init()
    {
        ////////////////////////////////////////////
        // iOS只能在App自己的資料夾底下創建檔案
        // 所以要先自己取得App的檔案路徑
        
        // get path for DocumentDirectory
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true);
        // create sqlpath
        var sqlpath = paths[0].stringByAppendingPathComponent(DBManager.DATABASE_NAME)
        
        m_db = Database(path:sqlpath)
        if let db = m_db
        {
            if db.schema.tables.count == 0
            {
                IniitlizeDataBase()
            }
        }
    }
    
    
    private func IniitlizeDataBase()
    {
        if let db = m_db
        {
            // Create Local Remote Data Base
            db.createTable(DBManager.REMOTE_TABLE,
                definitions:[
                    "Name TEXT NOT NULL Primary Key",
                    "IsUserDefine BOOL",
                    "BoardMAC TEXT",
                    "DeviceID TEXT NOT NULL",
                    "BrandID TEXT",
                    "CodeNum TEXT"
                ])
            
            db.createTable(DBManager.BRAND_TABLE,
                definitions:[
                    DBManager.DBTAG_BRANDID+" TEXT Primary Key",
                    DBManager.DBTAG_BRANDNAME+" TEXT NOT NULL"
                ])
            
            db.createTable(DBManager.IR_TABLE,
                definitions:[
                    DBManager.DBTAG_CODENUM+" TEXT NOT NULL",
                    DBManager.DBTAG_KEYID+" TEXT",
                    DBManager.DBTAG_IRDATA+" TEXT"
                ])
            db.createTable(DBManager.IR_AC_TABLE,
                definitions:[
                    "CodeNum TEXT NOT NULL",
                    "KeyID TEXT",
                    "stPower TEXT",
                    "stMode TEXT",
                    "stTemp TEXT",
                    "stFan TEXT",
                    "stSwing TEXT",
                    "IRData TEXT"
                ])
            db.createTable(DBManager.MODIFY_IRDATA_TABLE,
                definitions:[
                    "Name TEXT",
                    "KeyID TEXT",
                    "IRData TEXT"
                ])
            
            db.createTable(DBManager.CODESET_TABLE,
                definitions:[
                    "DeviceID TEXT",
                    "BrandID TEXT",
                    "CodeNum TEXT",
                    "CodeName TEXT"
                ])
            
            db.createTable(DBManager.KEY_TABLE,
                definitions:[
                    "KeyID TEXT NOT NULL",
                    "KeyName TEXT"
                ])
            
            db.createTable(DBManager.KEY_AC_TABLE,
                definitions:[
                    "KeyID TEXT NOT NULL",
                    "KeyName TEXT"
                ])
            
            db.createTable(DBManager.SERVICE_TABLE,
                definitions:[
                    "ServiceName TEXT NOT NULL Primary Key",
                    "Type TEXT",
                    "Domain TEXT"
                ])
            
        }
    }
    
    func SaveRemote(pRemote : Remote, learning_list: Array<AnyObject>) -> SaveStatus
    {
        ////// insert remote data
        var result = insert_to_table(DBManager.REMOTE_TABLE, wherecolumns: nil, insertvalues: [DBManager.DBTAG_REMOTENAME : pRemote.Name,
            DBManager.DBTAG_USERDEFINE : false,
            DBManager.DBTAG_BOARDMAC : "11111",
            DBManager.DBTAG_DEVICEID : pRemote.DeviceID,
            DBManager.DBTAG_BRANDID  : pRemote.BrandID,
            DBManager.DBTAG_CODENUM  : pRemote.CodeNum])
        
        if result != SaveStatus.Sucess
        {
            return result
        }
        
        
        ////// update brand table
        var whcolumns = [DBManager.DBTAG_BRANDID+"="+pRemote.BrandID]
        var brandtable_result = insert_to_table(DBManager.BRAND_TABLE, wherecolumns: whcolumns,
            insertvalues: [DBManager.DBTAG_BRANDNAME:pRemote.BrandName, DBManager.DBTAG_BRANDID:pRemote.BrandID])
        
        if brandtable_result != SaveStatus.Sucess
        {
            return brandtable_result
        }
        
        
        ////// update codeset table
        whcolumns = [DBManager.DBTAG_DEVICEID+"="+pRemote.DeviceID,
            DBManager.DBTAG_BRANDID+"="+pRemote.BrandID,
            DBManager.DBTAG_CODENUM+"="+pRemote.CodeNum]
        var codeset_result = insert_to_table(DBManager.CODESET_TABLE, wherecolumns: whcolumns,
            insertvalues: [DBManager.DBTAG_DEVICEID:pRemote.DeviceID,
                DBManager.DBTAG_BRANDID:pRemote.BrandID,
                DBManager.DBTAG_CODENUM:pRemote.CodeNum,
                DBManager.DBTAG_CODENAME:pRemote.CodeName])
        
        if codeset_result != SaveStatus.Sucess
        {
            return codeset_result
        }
        
        
        ////////////////////////////////////////////
        ////// update key table
        for key in pRemote.Keys
        {
            ////// update key table
            whcolumns = [DBManager.DBTAG_KEYID+"="+key.KeyID]
            var key_result = insert_to_table(DBManager.KEY_TABLE, wherecolumns: whcolumns,
                insertvalues: [DBManager.DBTAG_KEYID:key.KeyID, DBManager.DBTAG_KEYNAME:key.KeyName])
            
            if key_result != SaveStatus.Sucess
            {
                return key_result
            }
            
            ////// update IR table
            whcolumns = [DBManager.DBTAG_CODENUM+"="+pRemote.CodeNum, DBManager.DBTAG_KEYID+"="+key.KeyID]
            var irtable_result = insert_to_table(DBManager.IR_TABLE, wherecolumns: whcolumns,
                insertvalues: [DBManager.DBTAG_CODENUM : pRemote.CodeNum,
                    DBManager.DBTAG_KEYID:key.KeyID,
                    DBManager.DBTAG_IRDATA:key.IRData])
            
            if irtable_result != SaveStatus.Sucess
            {
                return irtable_result
            }
        }
        
        for key in pRemote.ACKeys
        {
            whcolumns = [DBManager.DBTAG_KEYID+"="+key.KeyID]
            var key_result = insert_to_table(DBManager.KEY_AC_TABLE, wherecolumns: whcolumns,
                insertvalues:[DBManager.DBTAG_KEYID:key.KeyID, DBManager.DBTAG_KEYNAME:key.KeyName])
            
            if key_result != SaveStatus.Sucess
            {
                return key_result
            }
            
            
            ////// update IR_AC table
            whcolumns = [DBManager.DBTAG_CODENUM+"="+pRemote.CodeNum, DBManager.DBTAG_KEYID+"="+key.KeyID]
            var irtable_result = insert_to_table(DBManager.IR_AC_TABLE, wherecolumns: whcolumns,
                insertvalues:[DBManager.DBTAG_CODENUM:pRemote.CodeNum,
                    DBManager.DBTAG_KEYID:key.KeyID, DBManager.DBTAG_STPOWER:key.Power, DBManager.DBTAG_STMODE:key.ModeStatus.description, DBManager.DBTAG_STTEMP:key.Temperature, DBManager.DBTAG_STFAN:key.Fanstatus.description, DBManager.DBTAG_STSWING:key.Swing, DBManager.DBTAG_IRDATA:key.IRData])
            
            if irtable_result != SaveStatus.Sucess
            {
                return irtable_result
            }
        }
        
        
        /////////////update learning key
        //
        if learning_list.count > 0
        {
            if let keylist = learning_list as? Array<RemoteKey>
            {
                for key in keylist
                {
                    
                    whcolumns = [DBManager.DBTAG_REMOTENAME+"="+pRemote.Name, DBManager.DBTAG_KEYID+"="+key.KeyID]
                    var save_learning_result = insert_to_table(DBManager.MODIFY_IRDATA_TABLE, wherecolumns: whcolumns,
                        insertvalues:[DBManager.DBTAG_KEYID:key.KeyID, DBManager.DBTAG_KEYNAME:key.KeyName])
                    
                    if save_learning_result != SaveStatus.Sucess
                    {
                        return save_learning_result
                    }
                }
            }
        }
        
        
        return SaveStatus.Sucess
    }
    
    func SaveIRService(servicename:String, type:String, domain:String) -> SaveStatus
    {
        println("SAVING: \(servicename) \(type) \(domain)")
        var whcolumns = [DBManager.DBTAG_SERVICENAME+"='"+servicename+"'"]
        
        var result = insert_to_table(DBManager.SERVICE_TABLE, wherecolumns: whcolumns,
            insertvalues:[DBManager.DBTAG_SERVICENAME:servicename,
                DBManager.DBTAG_SERVICETYPE:type,
                DBManager.DBTAG_SERVICEDOMAIN:domain])
        
        return result
    }
    
    /*
    得到全部的IR Service List
    return Array<Array<String>>: [[name1, type, domain], [name2, type, domain], [name3, type, domain]....]
    */
    func GetAllIRServiceList()->Array<Array<String>>
    {
        var list = Array<Array<String>>()
        var error:NSError?
        for row in m_db.selectFrom(DBManager.SERVICE_TABLE, error:&error) {
            
            if row == nil {
                // handle error
                println("Get IR Service List Error = \(error?.code)")
                break
            }
            
            var temp = Array<String>()
            temp.append(row!.stringValue(DBManager.DBTAG_SERVICENAME)!)
            temp.append(row!.stringValue(DBManager.DBTAG_SERVICETYPE)!)
            temp.append(row!.stringValue(DBManager.DBTAG_SERVICEDOMAIN)!)
            list.append(temp)
        }
        return list
    }
    
    //    func SaveRemote(pRemote : Remote, learning_list: Array<AnyObject>) -> SaveStatus
    //    {
    //        ////// insert remote data
    //        var error: NSError?
    //        if let rowId = m_db.insertInto(DBManager.REMOTE_TABLE, values:[DBManager.DBTAG_REMOTENAME : pRemote.Name,
    //            DBManager.DBTAG_USERDEFINE : false,
    //            DBManager.DBTAG_BOARDMAC : "11111",
    //            DBManager.DBTAG_DEVICEID : pRemote.DeviceID,
    //            DBManager.DBTAG_BRANDID  : pRemote.BrandID,
    //            DBManager.DBTAG_CODENUM  : pRemote.CodeNum]
    //            ,error: &error)
    //        {
    //
    //        } else {
    //            // handle error
    //            // Remote Name Duplicate
    //            if(error?.code == 19){
    //                return SaveStatus.Repeate
    //            }
    //            else{
    //                return SaveStatus.Error
    //            }
    //        }
    //
    //        ////// update brand table
    //        var count_brand_error: NSError?
    //        var whStatement = DBManager.DBTAG_BRANDNAME+"='"+pRemote.BrandName+"'"
    //        whStatement = whStatement+" AND "+DBManager.DBTAG_BRANDID+"="+pRemote.BrandID
    //
    //        if m_db.countFrom(DBManager.BRAND_TABLE, whereExpr: whStatement, error:&count_brand_error) == 0
    //        {
    //            var insert_brand_error: NSError?
    //            if let rowId = m_db.insertInto(DBManager.BRAND_TABLE,
    //                values:[DBManager.DBTAG_BRANDNAME:pRemote.BrandName, DBManager.DBTAG_BRANDID:pRemote.BrandID]
    //                ,error: &insert_brand_error)
    //            {
    //
    //            }
    //            else {
    //                println("insert brand table error=\(insert_brand_error?.code)")
    //                return SaveStatus.Error
    //            }
    //        }
    //
    //        ////// update codeset table
    //        var count_codeset_error: NSError?
    //        whStatement = DBManager.DBTAG_DEVICEID+"="+pRemote.DeviceID
    //        whStatement = whStatement+" AND "+DBManager.DBTAG_BRANDID+"="+pRemote.BrandID
    //        whStatement = whStatement+" AND "+DBManager.DBTAG_CODENUM+"="+pRemote.CodeNum
    //
    //        if m_db.countFrom(DBManager.CODESET_TABLE, whereExpr: whStatement, error:&count_codeset_error) == 0
    //        {
    //            var insert_codeset_error: NSError?
    //            if let rowId = m_db.insertInto(DBManager.CODESET_TABLE,
    //                values:[DBManager.DBTAG_DEVICEID:pRemote.DeviceID,
    //                    DBManager.DBTAG_BRANDID:pRemote.BrandID,
    //                    DBManager.DBTAG_CODENUM:pRemote.CodeNum,
    //                    DBManager.DBTAG_CODENAME:pRemote.CodeName]
    //                ,error: &insert_codeset_error)
    //            {
    //
    //            }
    //            else {
    //                println("insert codeset error=\(insert_codeset_error?.code)")
    //                return SaveStatus.Error
    //            }
    //        }
    //
    //        ////////////////////////////////////////////
    //        ////// update key table
    //        for key in pRemote.Keys
    //        {
    //            ////// update key table
    //            var count_key_error: NSError?
    //            whStatement = DBManager.DBTAG_KEYID+"="+key.KeyID
    //            whStatement = whStatement+" AND "+DBManager.DBTAG_KEYNAME+"='"+key.KeyName+"'"
    //
    //            if m_db.countFrom(DBManager.KEY_TABLE, whereExpr: whStatement, error:&count_key_error) == 0
    //            {
    //                var insert_key_error: NSError?
    //                if let rowId = m_db.insertInto(DBManager.KEY_TABLE,
    //                    values:[DBManager.DBTAG_KEYID:key.KeyID, DBManager.DBTAG_KEYNAME:key.KeyName]
    //                    ,error: &insert_key_error) {
    //
    //                }
    //                else {
    //                    println("insert key table error=\(insert_key_error?.code)")
    //                    return SaveStatus.Error
    //                }
    //            }
    //
    //            ////// update IR table
    //            var count_ir_error: NSError?
    //            whStatement = DBManager.DBTAG_CODENUM+"="+pRemote.CodeNum
    //            whStatement = whStatement+" AND "+DBManager.DBTAG_KEYID+"="+key.KeyID
    //
    //            if m_db.countFrom(DBManager.IR_TABLE, whereExpr: whStatement, error:&count_ir_error) == 0
    //            {
    //                var insert_ir_error: NSError?
    //                if let rowId = m_db.insertInto(DBManager.IR_TABLE,
    //                    values:[DBManager.DBTAG_CODENUM : pRemote.CodeNum,
    //                        DBManager.DBTAG_KEYID:key.KeyID,
    //                        DBManager.DBTAG_IRDATA:key.IRData]
    //                    ,error: &insert_ir_error) {
    //
    //                }
    //                else {
    //                    println("insert ir table error=\(insert_ir_error?.code)")
    //                    return SaveStatus.Error
    //                }
    //            }
    //
    //        }
    //
    //        for key in pRemote.ACKeys
    //        {
    //            var count_codeset_error: NSError?
    //            whStatement = DBManager.DBTAG_KEYID+"="+key.KeyID
    //            whStatement = whStatement+" AND "+DBManager.DBTAG_KEYNAME+"="+key.KeyName
    //
    //            if m_db.countFrom(DBManager.KEY_AC_TABLE, whereExpr: whStatement, error:&count_codeset_error) == 0
    //            {
    //                var insert_codeset_error: NSError?
    //                if let rowId = m_db.insertInto(DBManager.KEY_AC_TABLE,
    //                    values:[DBManager.DBTAG_KEYID:key.KeyID,
    //                        DBManager.DBTAG_KEYNAME:key.KeyName]
    //                    ,error: &insert_codeset_error) {
    //
    //                }
    //                else {
    //                    println(insert_codeset_error?.code)
    //                    return SaveStatus.Error
    //                }
    //            }
    //
    //            ////// update IR_AC table
    //            var count_ir_error: NSError?
    //            whStatement = DBManager.DBTAG_CODENUM+"="+pRemote.CodeNum
    //            whStatement = whStatement+" AND "+DBManager.DBTAG_KEYID+"="+key.KeyID
    //
    //            if m_db.countFrom(DBManager.IR_AC_TABLE, whereExpr: whStatement, error:&count_ir_error) == 0
    //            {
    //                var insert_ir_error: NSError?
    //                if let rowId = m_db.insertInto(DBManager.IR_AC_TABLE,
    //                    values:[DBManager.DBTAG_CODENUM : pRemote.CodeNum,
    //                        DBManager.DBTAG_KEYID:key.KeyID,
    //                        DBManager.DBTAG_KEYID:key.KeyID,
    //                        DBManager.DBTAG_KEYID:key.KeyID,
    //                        DBManager.DBTAG_KEYID:key.KeyID,
    //                        DBManager.DBTAG_KEYID:key.KeyID,
    //                        DBManager.DBTAG_KEYID:key.KeyID,
    //                        DBManager.DBTAG_IRDATA:key.IRData]
    //                    ,error: &insert_ir_error) {
    //
    //                }
    //                else {
    //                    println("insert ir table error=\(insert_ir_error?.code)")
    //                    return SaveStatus.Error
    //                }
    //            }
    //
    //
    //        }
    //
    //
    //        /////////////update learning key
    //        //
    //        if learning_list.count > 0
    //        {
    //            if let keylist = learning_list as? Array<RemoteKey>
    //            {
    //                for key in keylist
    //                {
    //                    var count_learnkey_error: NSError?
    //                    var whStatement = DBManager.DBTAG_REMOTENAME+"="+pRemote.Name
    //                    whStatement = whStatement+" AND "+DBManager.DBTAG_KEYID+"="+key.KeyID
    //
    //                    if m_db.countFrom(DBManager.MODIFY_IRDATA_TABLE, whereExpr: whStatement, error:&count_learnkey_error) == 0
    //                    {
    //                        var insert_learnkey_error: NSError?
    //                        if let rowId = m_db.insertInto(DBManager.MODIFY_IRDATA_TABLE,
    //                            values:[DBManager.DBTAG_KEYID:key.KeyID, DBManager.DBTAG_KEYNAME:key.KeyName]
    //                            ,error: &insert_learnkey_error) {
    //                        }
    //                        else {
    //                            println("insert modify key table error=\(insert_learnkey_error?.code)")
    //                            return SaveStatus.Error
    //                        }
    //
    //                    }else {
    //                        var error: NSError?
    //                        if let updateCount = m_db.update(DBManager.MODIFY_IRDATA_TABLE,
    //                            set:[DBManager.DBTAG_IRDATA:key.IRData], whereExpr: whStatement, error: &error) {
    //                        } else {
    //                            println("update modify key table error=\(error?.code)")
    //                            return SaveStatus.Error
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //
    //
    //
    //        return SaveStatus.Sucess
    //    }
    
    /*
    insert row data into table, 如果row data已經存在會改成update
    */
    func insert_to_table(tablename:String, wherecolumns:Array<String>?, insertvalues:[String:Bindable?])->SaveStatus
    {
        
        var count_error: NSError?
        if let columns = wherecolumns
        {
            var whStatement :String = columns[0]
            for var x = 1 ; x < columns.count ; x++
            {
                whStatement = whStatement+" AND "+columns[x]
            }
            
            if m_db.countFrom(tablename, whereExpr: whStatement, error:&count_error) == 0
            {
                var insert_error: NSError?
                if let rowId = m_db.insertInto(tablename, values: insertvalues, error: &insert_error) {
                }
                else {
                    println("insert table: \(tablename) error=\(insert_error?.code)")
                    return SaveStatus.Error
                }
                
            }else {
                var update_error: NSError?
                if let updateCount = m_db.update(tablename,
                    set:insertvalues, whereExpr: whStatement, error: &update_error) {
                } else {
                    println("update table: \(tablename) error=\(update_error?.code)")
                    return SaveStatus.Error
                }
            }
        }
        else
        {
            var insert_error: NSError?
            if let rowId = m_db.insertInto(tablename, values: insertvalues, error: &insert_error) {
            }
            else {
                println("insert modify key table: \(tablename) error=\(insert_error?.code)")
                return SaveStatus.Error
            }
        }
        return SaveStatus.Sucess
    }
    
    
    func LoadRemote(RemoteName : String) -> Remote
    {
        var pReturn : Remote!
        var error:NSError?
        
        for row in m_db.selectFrom(DBManager.REMOTE_TABLE, error:&error) {
            
            if row == nil {
                // handle error
                break
            }
            println("LoadRemote: \(row!.dictionaryValue)")     // read the whole row as a Dictionary
            
            //Load Remote Data
            pReturn = Remote()
            pReturn.DeviceID = row!.stringValue(DBManager.DBTAG_DEVICEID)!
            pReturn.CodeNum = row!.stringValue(DBManager.DBTAG_CODENUM)!
            pReturn.Name = RemoteName
            pReturn.BrandID = row!.stringValue(DBManager.DBTAG_BRANDID)!
            
            // get codename
            var str = DBManager.DBTAG_DEVICEID+"="+pReturn.DeviceID
            str = str+" AND "+DBManager.DBTAG_BRANDID+"="+pReturn.BrandID
            str = str+" AND "+DBManager.DBTAG_CODENUM+"="+pReturn.CodeNum
            for codename in m_db.selectFrom(DBManager.CODESET_TABLE, whereExpr:str, error:&error) {
                pReturn.CodeName = codename!.stringValue(DBManager.DBTAG_CODENAME)!
            }
            
            //get brandname
            for brandname in m_db.selectFrom(DBManager.BRAND_TABLE, whereExpr: DBManager.DBTAG_BRANDID+"="+pReturn.BrandID, error:&error) {
                pReturn.BrandName = brandname!.stringValue(DBManager.DBTAG_BRANDNAME)!
            }
            
            //get ir data
            if pReturn.DeviceID == GlobalVariable.DEVICE_ID_AIRCONDITIONER
            {
                pReturn.ACKeys = Array<RemoteKey_AC>()
                for ackey in m_db.selectFrom(DBManager.IR_AC_TABLE, whereExpr: DBManager.DBTAG_CODENUM+"="+pReturn.CodeNum, error:&error) {
                    var temp = RemoteKey_AC()
                    temp.KeyID = ackey!.stringValue(DBManager.DBTAG_KEYID)!
                    temp.Power = ackey!.boolValue(DBManager.DBTAG_STPOWER)!
                    temp.ModeStatus = RemoteKey_AC.AIRMODE(rawValue: ackey!.stringValue(DBManager.DBTAG_STMODE)!)!
                    
                    temp.Temperature = ackey!.intValue(DBManager.DBTAG_STTEMP)!
                    temp.Fanstatus =     RemoteKey_AC.AIRFAN(rawValue: ackey!.stringValue(DBManager.DBTAG_STFAN)!)!
                    temp.Swing = ackey!.boolValue(DBManager.DBTAG_STSWING)!
                    temp.IRData = ackey!.stringValue(DBManager.DBTAG_IRDATA)!
                    pReturn.ACKeys.append(temp)
                }
                
            }else {
                pReturn.Keys = Array<RemoteKey>()
                for key in m_db.selectFrom(DBManager.IR_TABLE, whereExpr: DBManager.DBTAG_CODENUM+"="+pReturn.CodeNum, error:&error) {
                    var temp = RemoteKey()
                    temp.KeyID = key!.stringValue(DBManager.DBTAG_KEYID)!
                    temp.IRData = key!.stringValue(DBManager.DBTAG_IRDATA)!
                    
                    for keyid in m_db.selectFrom(DBManager.KEY_TABLE, whereExpr: DBManager.DBTAG_KEYID+"="+temp.KeyID, error:&error) {
                        temp.KeyName = keyid!.stringValue(DBManager.DBTAG_KEYNAME)!
                    }
                    
                    pReturn.Keys.append(temp)
                }
            }
            
        }
        
        return pReturn
    }
    
    // Load all remotes in local database
    func LoadAllLocalRemotes()->Array<Remote>
    {
        var list = Array<Remote>()
        var error:NSError?
        
        println("local remote count = \(m_db.countFrom(DBManager.REMOTE_TABLE, error:&error))")
        
        for row in m_db.query("SELECT * FROM "+DBManager.REMOTE_TABLE, error:&error) {
            
            //Load Remote Data
            var pReturn = Remote()
            pReturn.DeviceID = row!.stringValue(DBManager.DBTAG_DEVICEID)!
            pReturn.CodeNum = row!.stringValue(DBManager.DBTAG_CODENUM)!
            pReturn.Name = row!.stringValue("Name")!
            pReturn.BrandID = row!.stringValue(DBManager.DBTAG_BRANDID)!
            
            list.append(pReturn)
        }
        
        return list
    }
    
    /*
    更新remote name
    */
    func RenameRemote(oldName: String, newName:String) -> SaveStatus
    {
        // update remote table
        var count_error: NSError?
        if m_db.countFrom(DBManager.REMOTE_TABLE, whereExpr: DBManager.DBTAG_REMOTENAME+"="+oldName, error:&count_error) == 0
        {
            println("rename remote: \(oldName) error=\(count_error?.code), no such remote")
            return SaveStatus.Error
            
        }else {
            var update_error: NSError?
            if let updateCount = m_db.update(DBManager.REMOTE_TABLE,
                set:[DBManager.DBTAG_REMOTENAME: newName,
                    DBManager.DBTAG_USERDEFINE : false,
                    DBManager.DBTAG_BOARDMAC : "11111",
                    DBManager.DBTAG_DEVICEID : "1",
                    DBManager.DBTAG_BRANDID  : "3345678",
                    DBManager.DBTAG_CODENUM  : "8088"],
                whereExpr: DBManager.DBTAG_REMOTENAME+"='"+oldName+"'", error: &update_error) {
            } else {
                println("update remote name: \(oldName) error=\(update_error?.code)")
                return SaveStatus.Error
            }
        }
        
        // update modify ir data table
        if m_db.countFrom(DBManager.MODIFY_IRDATA_TABLE, whereExpr: DBManager.DBTAG_REMOTENAME+"='"+oldName+"'", error:&count_error) == 0
        {
            
        }else {
            var update_error: NSError?
            if let updateCount = m_db.update(DBManager.MODIFY_IRDATA_TABLE,
                set:[DBManager.DBTAG_REMOTENAME: newName],
                whereExpr: DBManager.DBTAG_REMOTENAME+"='"+oldName+"'", error: &update_error) {
            } else {
                println("update remote(modify key table) name: \(oldName) error=\(update_error?.code)")
                return SaveStatus.Error
            }
        }
        
        
        return SaveStatus.Sucess
    }
    
    /*
    * 刪除remote
    */
    func DeleteRemote(remoteName: String) -> Bool
    {
        //delete remote
        var error: NSError?
        if let deleteCount = m_db.deleteFrom(DBManager.REMOTE_TABLE,
            whereExpr: DBManager.DBTAG_REMOTENAME+"='"+remoteName+"'", error:&error) {
                
                // deleteCount is the number of deleted rows
                return true
                
        } else {
            println("delete remote error = \(error?.code)")
            return false
        }
    }
    
    func SaveSchedule(RemoteName : String) -> SaveStatus
    {
        return SaveStatus.Sucess
    }
    
    //顯示Schedule List
    func LoadSchedule(RemoteName : String) -> Array<String>
    {
        return Array<String>()
    }
    
    func SaveIRExtender(IRName : String) -> Bool
    {
        return false
    }
    
    func LoadIRExtender(IRName : String) -> Bool
    {
        return false
    }
    
    //
    static func DEVICE_IMG_MAPPING() -> Dictionary<String, String>
    {
        var mapping = Dictionary<String, String>()
        
        mapping.updateValue("ic_aircondition01.png", forKey: GlobalVariable.DEVICE_ID_AIRCONDITIONER)
        mapping.updateValue("ic_tv.png", forKey: GlobalVariable.DEVICE_ID_TV)
        mapping.updateValue("ic_vhs.png", forKey: GlobalVariable.DEVICE_ID_VHS)
        mapping.updateValue("ic_dvd_player.png", forKey: GlobalVariable.DEVICE_ID_DVD)
        mapping.updateValue("ic_audio.png", forKey: GlobalVariable.DEVICE_ID_AUDIO)
        mapping.updateValue("ic_cd_player.png", forKey: GlobalVariable.DEVICE_ID_CD)
        mapping.updateValue("ic_other_device", forKey: GlobalVariable.DEVICE_ID_OTHER)
        mapping.updateValue("ic_streaming_box.png", forKey: GlobalVariable.DEVICE_ID_STREAMING)
        mapping.updateValue("ic_stb01.png", forKey: GlobalVariable.DEVICE_ID_STB)
        mapping.updateValue("ic_projector.png", forKey: GlobalVariable.DEVICE_ID_PROJECTOR)
        mapping.updateValue("ic_ud01.png", forKey: GlobalVariable.DEVICE_ID_USER_AV)
        mapping.updateValue("ic_userdefine_ac.png", forKey: GlobalVariable.DEVICE_ID_USER_AC)
        
        return mapping
    }
    
    
}