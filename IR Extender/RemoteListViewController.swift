//
//  RemoteListViewController.swift
//  IR Extender GIT
//
//  Created by Foxconn CPEIOT on 2015/8/3.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Henry 2015-08-03 Created
//////////////////////////////////////////////
import Foundation

class RemoteListViewController: UITableViewController, UITableViewDelegate {
    
    var deviceName = "string"
    @IBOutlet var remoteListView: UITableView!
    var displayListDataSource: CustomDatasource!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        displayListDataSource = CustomDatasource(cellData: Array<ImageTitleCellData>(), cellIdentifier: CustomDatasource.textCellIdentifier,
            configureCell:{(cell, celldata) in
                var deviceListCell = cell as! ImageTitleCell;
                deviceListCell.configureForCell(celldata as! ImageTitleCellData);
        });
        remoteListView.dataSource = displayListDataSource
        remoteListView.delegate = self

        println("\(deviceName)")
    }

}