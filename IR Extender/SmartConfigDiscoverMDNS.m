//
//  Copyright (c) 2014 Texas Instruments. All rights reserved.
//


#import "SmartConfigDiscoverMDNS.h"
#import "IR_Extender-Swift.h"

@implementation SmartConfigDiscoverMDNS
static SmartConfigDiscoverMDNS *instance =nil;

+ (SmartConfigDiscoverMDNS *)getInstance
{
    NSLog(@"SmartConfigDiscoverMDNS getinstance");
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [SmartConfigDiscoverMDNS new];
            instance.globalConfig = [SmartConfigGlobalConfig getInstance];
        }
    }
    return instance;
}

- (void) emptyMDNSList
{
    [self.globalConfig emptyDeviceList];
}


- (void) startMDNSDiscovery:(NSString*)deviceName {
    NSLog(@"Starting mDNS discovery with device name %@", deviceName);
    
    NSLog(@"Stop discovery prior to starting again");
    [self stopMDNSDiscovery];
    
    self.deviceName = deviceName;
    self.netServiceBrowser = [[NSNetServiceBrowser alloc] init];
    [self.netServiceBrowser setDelegate:self];
    [self.netServiceBrowser searchForServicesOfType:@"_http._tcp" inDomain:@""];
}


-(void) stopMDNSDiscovery {
    NSLog(@"Stop mDNS discovery");

    [self.netServiceBrowser stop];
}

/* callback when found device */
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing
{
//    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
//    NSLog(@"%@ %@ %@ %@ %@", aNetService.name, aNetService.hostName, aNetService.addresses, aNetService.type, aNetService.domain);
    if (!self.netServices) {
        self.netServices = [[NSMutableArray alloc] init];
    }
    [self.netServices addObject:aNetService];
    
    [[self.netServices objectAtIndex:([self.netServices count] -1)] setDelegate:self];
    
    [[self.netServices objectAtIndex:([self.netServices count] -1)] resolveWithTimeout:20.0];
    
    // rest of logic is in the resolve callback
    
}

////henry20150715, callback when device is not found
//- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing;
//{
//    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
//    NSLog(@"didRemoveService %@", aNetService.name);
//}
//
//- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict
//{
//    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
//    NSLog(@"%@", sender);
//    NSLog(@"%@", errorDict);
//}
//
//- (void)netServiceWillPublish:(NSNetService *)sender
//{
//    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
//    NSLog(@"%@", sender);
//    
//}
//
//- (void)netServiceWillResolve:(NSNetService *)sender
//{
//    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
//    NSLog(@"%@", sender);
//    
//}
//
//- (void)netServiceDidStop:(NSNetService *)sender {
//    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
//    NSLog(@"address: %@", sender.name);
////    NSLog(@"%@",[self.globalConfig getDevices]);
////    [self.globalConfig removeDevice:sender.name];
////    NSLog(@"%@",[self.globalConfig getDevices]);
////
////    [self.NotifyMdnsCompletion NotifyMdnsCompletion:self];  //henry20150713, emit signal to controller when device was added
//
//    
//}

// logic after the service was resolved, here we add it to devices
- (void)netServiceDidResolveAddress:(NSNetService *)aNetService {
    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
    
    // split by @ to get the device name
    NSArray* serviceNameParts = [aNetService.name componentsSeparatedByString: @"@"];
    NSLog(@"netServiceDidResolveAddress:%@ %@ %@ %@ %@", aNetService.name, aNetService.hostName, aNetService.addresses, aNetService.type, aNetService.domain);
    
    // GET DEVICE INFO
    NSString *key = aNetService.name;   //henry20150708, use device name as key
//    NSString *key = [self resolveAddress:aNetService.addresses];    //henry20150708, use ip as key to avoid identical device name will only detect once
    NSDictionary *device = [NSMutableDictionary dictionaryWithCapacity:3];
    NSString *address = [self resolveAddress:aNetService.addresses];
    NSDictionary *dataDict = [NSNetService dictionaryFromTXTRecordData:aNetService.TXTRecordData];
    NSString *srcvers = @"";
    if(dataDict != nil && [dataDict valueForKey:@"srcvers"] != nil)
    {
        srcvers = [[NSString alloc] initWithData:[dataDict valueForKey:@"srcvers"] encoding:NSUTF8StringEncoding];
    }
    
    NSString *expectedSrcvers = @"1D90645";
    
    // if device name exists and the device name equals to the service that we discovered
    // or device name is blank and srcvers is equal to vers
    BOOL foundDevice =  ([self.deviceName length]
                         && [ [serviceNameParts objectAtIndex:([serviceNameParts count] - 1)] isEqualToString:self.deviceName ])
    || (![self.deviceName length] && [srcvers length] && [srcvers isEqualToString:expectedSrcvers]);
    //    [self.deviceName.text length] &&
    if([serviceNameParts count] > 0 && foundDevice)
//    if([serviceNameParts count] > 0)
    {
//        NSLog(@"inside if");

        [device setValue:aNetService.name forKey:@"name"];
        NSDate *now = [NSDate date];
        [device setValue:now forKey:@"date"];
        [device setValue:address forKey:@"url"];
        [device setValue:aNetService.type forKey:@"type"];  //henry20150721, add type to device dictionary
        [device setValue:aNetService.domain forKey:@"domain"];  //henry20150721, add domain to device dictionary

        // set recent flag if device name exists and we added it from smartconfig
        if([self.deviceName length])
        {
            [device setValue:[NSNumber numberWithBool:YES] forKey:@"recent"];
        }
        else
        {
            [device setValue:[NSNumber numberWithBool:NO] forKey:@"recent"];
            
        }
        
        NSUInteger deviceCount = [[self.globalConfig getDevices] count];

        [self.globalConfig addDevice:device withKey:key];
//        NSLog(@"%lu %lu", [[self.globalConfig getDevices] count], (unsigned long)deviceCount);

        // check if we have more than one device
        if([[self.globalConfig getDevices] count] > deviceCount)
        {
            // a new device was added
//            NSLog(@"device added inside if %@", [self.globalConfig deviceName]);

            [self deviceWasAdded];
        }
    }
    
    [self.NotifyMdnsCompletion NotifyMdnsCompletion:self];  //henry20150713, emit signal to controller when device was added
}


-(NSString*) resolveAddress:(NSArray*)addresses {
    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
    char addressBuffer[INET6_ADDRSTRLEN];
    NSString *address;
    for (NSData *data in addresses)
    {
        memset(addressBuffer, 0, INET6_ADDRSTRLEN);
        
        typedef union {
            struct sockaddr sa;
            struct sockaddr_in ipv4;
            struct sockaddr_in6 ipv6;
        } ip_socket_address;
        
        ip_socket_address *socketAddress = (ip_socket_address *)[data bytes];
        
        if (socketAddress && (socketAddress->sa.sa_family == AF_INET || socketAddress->sa.sa_family == AF_INET6))
        {
            const char *addressStr = inet_ntop(
                                               socketAddress->sa.sa_family,
                                               (socketAddress->sa.sa_family == AF_INET ? (void *)&(socketAddress->ipv4.sin_addr) : (void *)&(socketAddress->ipv6.sin6_addr)),
                                               addressBuffer,
                                               sizeof(addressBuffer));
            
            int port = ntohs(socketAddress->sa.sa_family == AF_INET ? socketAddress->ipv4.sin_port : socketAddress->ipv6.sin6_port);
            
            if (addressStr && port)
            {
                address = [NSString stringWithFormat:@"http://%s:%d", addressStr, port];
            }
        }
    }
    
    return address;
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser didNotSearch:(NSDictionary *)errorInfo
{
    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
    NSLog(@"%@", errorInfo);
}

- (void)netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)netServiceBrowser {
    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
    NSLog(@"%@", netServiceBrowser);
    
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser didFindDomain:(NSString *)domainName moreComing:(BOOL)moreDomainsComing {
    NSLog(@"Callback %s", __PRETTY_FUNCTION__);
    
    NSLog(@"%@", domainName);
    
}

- (void) deviceWasAdded {
    NSLog(@"deviceWasAdded Callback %s", __PRETTY_FUNCTION__);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceFound" object:self];
}

@end
