//
//  MQTT_Brain.swift
//  IR_Extender_Demo
//
//  Created by Foxconn CPEIOT on 2015/6/12.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import Moscapsule
import Darwin

//henry20150624 add class for MQTT related functions
class MQTT_Brain {
    let IRDATA = "262A03C88801901ABA96462C792C64C27058486602DC63C8882101470010009378000000786F00004663006F467800786F46636F6300000000000000000000000000000000000000000000000000000000"
    
    struct Def {
//        static var mqttConfig = MQTTConfig(clientId: "iOS_ID1", host: GlobalVariable.MQTT_DEVICE_IP, port: Int32(GlobalVariable.MQTT_DEVICE_PORT.toInt()!), keepAlive: GlobalVariable.MQTT_KEEPALIVE)  //henry20150622, modify dynamic address:port
//        static var testCount = 0
//        static var MacAddress = 111111111111
        static var selfMacAddress = UIDevice.currentDevice().identifierForVendor.UUIDString[24...35]   //henry20150626, get last 12digit of UUID to simulate MAC Address of self
//
    }
    
    var mqttConfig:MQTTConfig!
    var mqtt_device_name: String!
    func setDeviceName(deviceName: String) {
        self.mqtt_device_name = deviceName
    }
    
    var mqttClient:MQTTClient!
    func connectToMQTT() {
//        var test = Connection.DEVICE_INFO
        var deviceAddress: String?
        var deviceName: String?
        
        if mqtt_device_name != GlobalVariable.MQTT_WAN_NAME {   //henry20150805, LAN
            deviceAddress = Connection.DEVICE_INFO[mqtt_device_name]?.Address
            deviceName = Connection.DEVICE_INFO[mqtt_device_name]?.Name
            println("\(deviceAddress!)")
            mqttConfig = MQTTConfig(clientId: "iOS_ID1", host: deviceAddress!, port: Int32(GlobalVariable.MQTT_LAN_PORT.toInt()!), keepAlive: GlobalVariable.MQTT_KEEPALIVE)  //henry20150622, modify dynamic address:port
            println("connecting \(deviceName) to \(deviceAddress):\(GlobalVariable.MQTT_LAN_PORT)")

        }
        else {  //henry20150805, WAN
            deviceName = GlobalVariable.MQTT_WAN_NAME
            deviceAddress = GlobalVariable.MQTT_WAN_ADDRESS
            mqttConfig = MQTTConfig(clientId: "iOS_ID1", host: GlobalVariable.MQTT_WAN_ADDRESS, port: Int32(GlobalVariable.MQTT_WAN_PORT.toInt()!), keepAlive: GlobalVariable.MQTT_KEEPALIVE)  //henry20150622, modify dynamic address:port
        }

        mqttConfig.onPublishCallback = { messageId in
            println("published \(messageId)")
        }
        mqttConfig.onMessageCallback = { mqttMessage in //henry20150809, message callback
//            println("\(mqttMessage.payloadString!) from \(mqttMessage.topic)")
            if (mqttMessage.topic as NSString).containsString("/Sta") == true {
                
                switch mqttMessage.payloadString! {
                case "BUSY":
                    println("BUSY from \(mqttMessage.topic)")
                case "IDLE":
                    println("IDLE from \(mqttMessage.topic)")
                default:
                    println("SOMETHING IS WRONG")
                }

            }
            else if (mqttMessage.topic as NSString).containsString("/Cmd") == true {
            }
            else if (mqttMessage.topic as NSString).containsString("/Rsp") == true {
                if mqttMessage.topic == "ERROR" {
                    println("RECEIVED PACKET IS NOT FORMATTED CORRECTLY")
                }
            }
            else if (mqttMessage.topic as NSString).containsString("/User") == true{
            }
        }
        
        //henry20150809, callback when server is connected, set isConnected flag to true
        mqttConfig.onConnectCallback = { returnCode in
            if returnCode == ReturnCode.Success {
                Connection.DEVICE_INFO[self.mqtt_device_name]?.isConnected = self.mqttClient.isConnected //expect true
                if Connection.DEVICE_INFO[self.mqtt_device_name]?.isConnected == true {
                    Connection.DEVICE_INFO[self.mqtt_device_name]?.isConnecting = false
                }
            }
        }
        
        //henry20150809, callback when server is disconnected, set isConnected flag to false
        mqttConfig.onDisconnectCallback = { reasonCode in
            Connection.DEVICE_INFO[self.mqtt_device_name]?.isConnected = self.mqttClient.isConnected //expect true
            if Connection.DEVICE_INFO[self.mqtt_device_name]?.isConnected == false {
                println("MQTT Disconnected!")
                Connection.DEVICE_INFO[self.mqtt_device_name]?.isConnecting = true
            }
        }
    
        mqttClient = MQTT.newConnection(mqttConfig) //comment: must register callback first!!!!

        
        MQTTtopic = Connection.getMacAddressFromName(deviceName!) //henry20150806, obtain Mac Address from device
        
        if Connection.DEVICE_INFO[mqtt_device_name]?.getConnectionType() == "LAN" { //henry20150806, subscribe only when LAN mode
            //henry20150806, subscribe to all topic types
            for topicTypes in Connection.topicType.allValues {
                var topic = MQTTtopic + topicTypes.rawValue //henry20150806, joins /IRDevices/[MAC]/ with Cmd/Rsp/Sta/User
                mqttClient?.subscribe(topic, qos: 2)
                println("\(topic)")
            }
        }
        
        println("mqttClient.isConnected: \(mqttClient.isConnected)")
        Connection.DEVICE_INFO[mqtt_device_name]?.isConnected = mqttClient.isConnected  //set status of mqtt, henry20150805

    }
    
    //henry20150626, create MQTT topic for specific device
    var topic:String = "faketopic"
    var MQTTtopic: String{
        get {
            return topic
        }
        
        set {
            topic = "IRDevices/\(newValue)/"
        }
    }
    
    /**
     * function to shape raw IR Data into working Packet
     * :isLearning: is the command a Learning command
     * :topicType: the type of the topic (Cmd/Sta/Rcv/User)
     * :toMAC: the MAC address of the device
     * :data: the IR Data
     */
    //henry20150805, add parameters to enable manipulation
    func publishToMQTT(isLearning:Bool, topicType:Connection.topicType, toMAC:String, data:String) {
        var packetHeader = "03"
        var topic: String
        if isLearning == true {
            packetHeader = "04"
        }

        if topicType.rawValue != "Other" {
            topic = "IRDevices/" + toMAC + "/" + topicType.rawValue
        }
        else {
            topic = "faketopic"
        }
        
        var sentData = packetHeader + Def.selfMacAddress.lowercaseString + toMAC + data

        
        //henry20150809, allow publish only when isConnected
        if Connection.DEVICE_INFO[mqtt_device_name]?.isConnected == true {
            mqttClient?.publishString(sentData, topic: topic, qos: GlobalVariable.MQTT_QOS, retain: GlobalVariable.MQTT_RETAIN)
            println("published IRDATA '\(sentData)' to \(topic) in \(Connection.DEVICE_INFO[mqtt_device_name]!.getConnectionType()) mode")
        }
        else {
            println("MQTT NOT CONNECTED")
        }
    }
    
    /**
        publish method for sharing data (SHARING function)
    */
    func publishToMQTT(userName: String, topicType:Connection.topicType, toMAC:String, data:String) {
        var packetHeader = "03"
        var topic: String
        
        if topicType.rawValue != "Other" {
            topic = "IRDevices/" + toMAC + "/" + topicType.rawValue
        }
        else {
            topic = "faketopic"
        }
        
        topic = topic + "/" + userName
        
        
        var sentData = packetHeader + Def.selfMacAddress.lowercaseString + toMAC + data
        
        //        var sentData = "03\(Def.selfMacAddress.lowercaseString)\(GlobalVariable.MQTT_DEVICE_MACADDRESS)\(data)"
        //        mqttClient?.publishString(sentData, topic: MQTTtopic, qos: GlobalVariable.MQTT_QOS, retain: GlobalVariable.MQTT_RETAIN)
        
        
        mqttClient?.publishString(sentData, topic: topic, qos: GlobalVariable.MQTT_QOS, retain: GlobalVariable.MQTT_RETAIN)
        println("\(mqttClient.isConnected) published IRDATA '\(sentData)' to \(MQTTtopic)")
    }
    
    //henry20150805
    func isConnected() -> Bool {
        return mqttClient.isConnected
    }
    
    
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[advance(self.startIndex, i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substringWithRange(Range(start: advance(startIndex, r.startIndex), end: advance(startIndex, r.endIndex)))
    }
}