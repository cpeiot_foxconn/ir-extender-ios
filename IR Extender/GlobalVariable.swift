//
//  GlobalVar.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/5/18.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  需要之公用變數可加在此處
//////////////////////////////////////////////

import Foundation
public class GlobalVariable{
    
    //////for getting Remote Data from DB
    public static let server_url = "http://smarthome.ambit.com.tw:5566/iot_irextender_20150616/"
    public static let url_all_devices = server_url + "get_all_devices.php"
    public static let url_all_brands = server_url + "get_all_brands.php"
    public static let url_codelist = server_url + "get_codelist.php"
    public static let url_irdata_av = server_url + "get_all_keys_av.php"
    public static let url_irdata_ac = server_url + "get_all_keys_ac.php"
    
    //////Json Data key name
    public static let DB_TABLE_DEVICENAME: String = "deviceName15"
    public static let DB_TABLE_DEVICEID: String = "deviceId"
    public static let DB_TABLE_BRANDNAME: String = "brandName"
    public static let DB_TABLE_BRANDID: String = "brandId"
    public static let DB_TABLE_CODENUM: String = "codeNum"
    public static let DB_TABLE_CODENAME: String = "modelNum"
    public static let DB_TABLE_KEYNAME: String = "keyLabel7"
    public static let DB_TABLE_KEYID: String = "keyId"
    public static let DB_TABLE_IRDATA: String = "irData"
    public static let DB_TABLE_FAN: String = "stFan"
    public static let DB_TABLE_SWING: String = "stSwing"
    public static let DB_TABLE_POWER: String = "stPower"
    public static let DB_TABLE_TEMPERATURE: String = "stTemp"
    public static let DB_TABLE_MODE: String = "stMode"
    
    
    //////Device ID List
    public static let DEVICE_ID_AIRCONDITIONER : String = "0"
    public static let DEVICE_ID_TV : String = "1"
    public static let DEVICE_ID_VHS : String = "2"
    public static let DEVICE_ID_DVD : String = "5"
    public static let DEVICE_ID_AUDIO : String = "6"
    public static let DEVICE_ID_CD : String = "7"
    public static let DEVICE_ID_OTHER : String = "8"
    public static let DEVICE_ID_STREAMING : String = "11"
    public static let DEVICE_ID_STB : String = "12"
    public static let DEVICE_ID_PROJECTOR : String = "36"
    public static let DEVICE_ID_USER_AV : String = "-1"
    public static let DEVICE_ID_USER_AC : String = "-2"
    
    //henry20150727, for timeout
    public static let TIMEOUT:NSTimeInterval = 3
    
    //MQTT Information
    static var MQTT_WAN_NAME = "IR-MQTT_WAN"
    static var MQTT_WAN_ADDRESS: String = "smarthome.ambit.com.tw"
    static var MQTT_WAN_PORT: String = "1883"
    static var MQTT_WAN_MAC: String = "111111111111"
    static var MQTT_LAN_PORT: String = "15000"
    static var MQTT_DEVICE_MACADDRESS: String = "f4b85e0151da"  //henry20150626, add mac address
    static var MQTT_TOPIC: String = "topic_ios"
    static var MQTT_QOS: Int32 = 0  //henry20150629, qos must be 0
    static var MQTT_RETAIN: Bool = false
    static var MQTT_KEEPALIVE:Int32 = 60
    

    static var DBMANAGER: DBManager!
}