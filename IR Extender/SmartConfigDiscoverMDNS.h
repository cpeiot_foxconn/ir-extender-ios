//
//  Copyright (c) 2014 Texas Instruments. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <netinet/in.h>
#include <arpa/inet.h>

//#import "IR_Extender-Swift.h"

#import "SmartConfigGlobalConfig.h"


//@class IrDeviceListViewController;
@protocol NotifyMdnsCompletionDelegate; //henry20150708, for delegate

@interface SmartConfigDiscoverMDNS : NSObject <NSNetServiceBrowserDelegate>
//- (id <NotifyMdnsCompletionDelegate>) NotifyMdnsCompletion;

@property (nonatomic, weak) id<NotifyMdnsCompletionDelegate> NotifyMdnsCompletion;
@property (nonatomic) NSNetServiceBrowser *netServiceBrowser;
@property SmartConfigGlobalConfig * globalConfig;
@property (retain) NSMutableArray *netServices;
@property (nonatomic, retain) NSString *deviceName;
+(SmartConfigDiscoverMDNS *)getInstance;
- (void) emptyMDNSList;
- (void) startMDNSDiscovery:(NSString*)deviceName;
- (void) stopMDNSDiscovery;
- (NSString*) resolveAddress:(NSArray*)addresses;   //henry20150728, allow public access
@end
