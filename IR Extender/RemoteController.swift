//
//  RemoteController.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/7/27.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Vinly 2015-07-29 Created
//
/*********************************************************************************
RemoteController: Handle Remote Save, Refresh, Delete, Homepage, Send IR Code
    - RemoteUI_Base: Display AV Device Remote UI
    - Remote_Air:    Display AC Device Remote UI
***********************************************************************************/

import UIKit
class RemoteController: UIViewController, NotifySendIRCodeDelegate{

    @IBOutlet weak var bar_save: UIBarButtonItem!
    @IBOutlet weak var bar_refresh: UIBarButtonItem!
    @IBOutlet weak var bar_rename: UIBarButtonItem!
    @IBOutlet weak var bar_homepage: UIBarButtonItem!
    @IBOutlet weak var bar_delete: UIBarButtonItem!
    
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var view_container: UIView!
    
    var m_bLocalRemote = false
    var m_pRemote: Remote!
    var viewcontroller_to_display : Remote_UIBase!
    var acviewcontroller_to_display : Remote_Air!
    var m_pMiddleController: RemoteMiddleController!
    var loadingview : CustomIOS7AlertView!
    
    var NotifyBakcToHomePage : NotifyEventDelegate!
    
    var IsLocalRemote: Bool {
        get{
            return m_bLocalRemote
        }
        set{
            m_bLocalRemote = newValue
            bar_delete.enabled = !m_bLocalRemote
            bar_rename.enabled = !m_bLocalRemote
        }
    }
   
    /////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if m_pRemote.DeviceID == GlobalVariable.DEVICE_ID_AIRCONDITIONER
        {
            acviewcontroller_to_display = m_pMiddleController.viewcontroller as! Remote_Air
        }else{
            viewcontroller_to_display = m_pMiddleController.viewcontroller as! Remote_UIBase
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func remote_delete(sender: UIBarButtonItem) {
    }

    func NotifySendIRCode(strIRData: String, bIsAVDevice: Bool) {
        println("sned ir code....")
    }
    
    @IBAction func remote_save(sender: UIBarButtonItem) {
        
        ////////if local remote
        if IsLocalRemote
        {
            // 直接儲存
            // update
            
        }
        else {
            ///////if new remote
            let alert_save = CustomIOS7AlertView()
            let saveview = SaveRemoteView()
            saveview.frame = CGRectMake(0,0, 250, 100)
            alert_save.containerView = saveview
            alert_save.buttonTitles = ["OK", "Cancel"]
            alert_save.onButtonTouchUpInside = {
                (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
                if(buttonIndex == 0)
                {
                    // if empty
                    if(saveview.tx_name.text == "")
                    {
                        self.simpleAlert("Empty", message: "Remote name can not be empty!", btnText: "OK").show()
                        return
                    }
                    
                    // if repeate
                    
                    //sucess
                    self.m_pRemote.Name = saveview.tx_name.text
                    
                    if(self.m_pRemote.DeviceID == GlobalVariable.DEVICE_ID_AIRCONDITIONER)
                    {
                        if GlobalVariable.DBMANAGER.SaveRemote(self.m_pRemote, learning_list: []) == DBManager.SaveStatus.Sucess
                        {
                            self.simpleAlert("Done", message: "Save Sucess!", btnText: "OK").show()
                        } else {
                            self.simpleAlert("Fail", message: "Save Fail! Please retry or restart app!", btnText: "OK").show()
                        }
                    }else
                    {
                        if GlobalVariable.DBMANAGER.SaveRemote(self.m_pRemote,
                            learning_list: self.viewcontroller_to_display.m_pLearningList) == DBManager.SaveStatus.Sucess
                        {
                            self.simpleAlert("Done", message: "Save Sucess!", btnText: "OK").show()
                        } else {
                            self.simpleAlert("Fail", message: "Save Fail! Please retry or restart app!", btnText: "OK").show()
                        }
                    }
                    
                    alert_save.close()
                }
                else
                {
                    alert_save.close()
                }
            }
            
            alert_save.show()
        }
    }
    
    @IBAction func remote_refresh_connection(sender: AnyObject) {
    }
    
    
    @IBAction func remote_rename(sender: AnyObject) {
    }
    
    
    @IBAction func remote_home(sender: AnyObject) {
        if let notify: NotifyEventDelegate = NotifyBakcToHomePage
        {
            notify.NotifyEventCompelete()
        }
    }
    
    func simpleAlert(title:String, message:String, btnText:String)->UIAlertView
    {
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButtonWithTitle(btnText)
        return alert
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        
        //display local remote
        if(segue.identifier == "segue_embed_remote")
        {
            var remote_display_controller : RemoteMiddleController = segue.destinationViewController as! RemoteMiddleController
            remote_display_controller.m_pRemote = self.m_pRemote
            remote_display_controller.NotifySendIRCode = self
            m_pMiddleController = remote_display_controller
        }
    }
}
