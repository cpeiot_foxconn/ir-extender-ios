//
//  deviceListViewController.swift
//  IR Extender
//
//  Created by Foxconn CPEIOT on 2015/7/14.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//
//////////////////////////////////////////////
//  Henry 2015-07-14 Created
//////////////////////////////////////////////

import Foundation
class DeviceListViewController: UIViewController {
//    let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var deviceListView: UIViewController!
    var navi: UINavigationController!
    var irDeviceListviewController: IrDeviceListViewController!
    
    @IBOutlet weak var bar_refresh_mdns: UIBarButtonItem!
    @IBOutlet weak var bar_back: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let story = UIStoryboard(name: "DeviceList", bundle: nil)
        
        deviceListView = story.instantiateViewControllerWithIdentifier("device_list_view") as! UIViewController
//        navi = UINavigationController(rootViewController: deviceListView)
//        appdelegate.window!.rootViewController = navi

        self.addChildViewController(deviceListView)
        self.view.addSubview(deviceListView.view)

    }
    
    @IBAction func back_navi(sender: UIBarButtonItem) {
        println("back_navi")
//        if let resultController = storyboard!.instantiateViewControllerWithIdentifier("main_view") as? UIViewController {
//            resultController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: "goBack")
//            let navController = UINavigationController(rootViewController: resultController) // Creating a navigation controller with VC1 at the root of the navigation stack.
//            self.presentViewController(navController, animated:true, completion: nil)
//        }
//        self.navigationController?.popToRootViewControllerAnimated(true)
        
//        self.navigationController?. (true)
    }
    
//    func goBack(){
//        println("goBack")
//        dismissViewControllerAnimated(true, completion: nil)
//    }
    
    @IBAction func refresh_mdns(sender: UIBarButtonItem) {
        println("refresh")
        IrDeviceListViewController().refresh_mdns()
    }
    
}