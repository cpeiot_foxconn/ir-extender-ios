//
//  Connection.swift
//  IR Extender GIT
//
//  Created by Foxconn CPEIOT on 2015/8/6.
//  Copyright (c) 2015年 Foxconn CPEIOT. All rights reserved.
//

import Foundation

public class Connection {
    static var DEVICE_INFO = [String:DeviceInfo]()  //henry20150805, dictionary for DeviceInfo for easy access
    
    enum topicType: String {
        case Cmd = "Cmd"
        case Sta = "Sta"
        case Rsp = "Rsp"
        case User = "User"
        case Other = "Other"
        
        static let allValues = [Cmd, Sta, Rsp, User]
    }

    //henry20150805
    /**
    returns the MAC Address of the given Device. If the given device does not have correct name schema [IR-XXXXXXXXXXXX], it will return default fake MAC [111111111111]
    */
    //TODO: save mac info into db
    static func getMacAddressFromName(deviceName: String) -> String{
        var MacAddress: String
        let arrayDeviceName = deviceName.componentsSeparatedByString("-")
        
        if arrayDeviceName[0] == "IR" {
            if count(arrayDeviceName[1]) == 12 {    //if suffix of deviceName seems to be Mac Address
                MacAddress = arrayDeviceName[1]
            }
            else {
                //                MacAddress = GlobalVariable.MQTT_WAN_MAC
                MacAddress = "222222222222"
                
            }
        }
        else {
            //            MacAddress = GlobalVariable.MQTT_WAN_MAC
            MacAddress = "333333333333"
            
        }
        
        return MacAddress
    }

    
}